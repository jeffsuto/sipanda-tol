<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Pelapor;
use App\Observers\PelaporObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setlocale('id');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
