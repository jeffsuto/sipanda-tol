<?php

namespace App\Observers;

use App\Models\Pelapor;

class PelaporObserver
{
    /**
     * Handle the pelapor "created" event.
     *
     * @param  \App\Models\Pelapor  $pelapor
     * @return void
     */
    public function created(Pelapor $pelapor)
    {
        //
    }

    public function saved(Pelapor $pelapor)
    {
        
    }

    /**
     * Handle the pelapor "updated" event.
     *
     * @param  \App\Models\Pelapor  $pelapor
     * @return void
     */
    public function updated(Pelapor $pelapor)
    {
        //
    }

    /**
     * Handle the pelapor "deleted" event.
     *
     * @param  \App\Models\Pelapor  $pelapor
     * @return void
     */
    public function deleted(Pelapor $pelapor)
    {
        //
    }

    /**
     * Handle the pelapor "restored" event.
     *
     * @param  \App\Models\Pelapor  $pelapor
     * @return void
     */
    public function restored(Pelapor $pelapor)
    {
        //
    }

    /**
     * Handle the pelapor "force deleted" event.
     *
     * @param  \App\Models\Pelapor  $pelapor
     * @return void
     */
    public function forceDeleted(Pelapor $pelapor)
    {
        //
    }
}
