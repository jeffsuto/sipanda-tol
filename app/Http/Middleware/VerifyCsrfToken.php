<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'mobile-login',
        'register-pelapor',
        'create-laporan-inspeksi',
        'check-status-dinas',
        'check-login',
        'data-info-lalu-lintas',
        'show-user',
        'create-akhir-dinas',
        'update-status-inspeksi',
        'check-existential-user',
        'create-dinas',
        'data-laporan-baru',
        'data-laporan-validasi',
        'data-riwayat-laporan',
        'update-status-laporan',
        'update-profile-pelapor',
        'last-data-akhir-dinas',
        'lupa-username',
        'reset-password',
        'update-profile',
        'data-kilometer-akhir-kendaraan'
    ];
}
