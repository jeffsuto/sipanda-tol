<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Dinas;

class DinasController extends Controller
{
    public function create(Request $request)
    {
        $dinas = new Dinas;
        $dinas->nik = $request->nik;
        $dinas->shift = $request->shift;
        $dinas->kendaraan = $request->kendaraan;
        
        if ($dinas->save()) {
            return response(['message' => 'success']);
        }else {
            return response(['message' => 'fail']);
        }
    }
}
