<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\AkhirDinas;
use App\Models\Ljt;
use App\Models\Pjr;

class AkhirDinasController extends Controller
{
    public function create(Request $request)
    {
        $ad = new AkhirDinas;
        $ad->nik = $request->nik;
        $ad->kendaraan = $request->kendaraan;
        $ad->kilometer_awal = $request->km_awal;
        $ad->kilometer_akhir = $request->km_akhir;
        $ad->kondisi = $request->kondisi;
        $ad->keterangan = $request->keterangan;

        $role = $request->role;
        
        if ($ad->save())
        {
            if ($role == 'LJT') {
                $ljt = Ljt::where('nik', $request->nik)->first();
                $ljt->logout_session = '1';
                
                if ($ljt->save())
                    return response(['message' => 'success']);
                else
                    return response(['message' => 'fail']);
            }else{
                $pjr = Pjr::where('nik', $request->nik)->first();
                $pjr->logout_session = '1';
                
                if ($pjr->save())
                    return response(['message' => 'success']);
                else
                    return response(['message' => 'fail']);
            }
        }
        else
        {
            return response(['message' => 'fail']);
        }
    }

    public function getLastDataAkhirDinasByNik(Request $request)
    {
        $ad = AkhirDinas::where('nik', $request->nik)->get()->last();

        if ($ad['nik'] != null) 
        {
            $data['message'] = "success";
            $data['data'] = AkhirDinas::where('nik', $request->nik)->get();
        }
        else
        {
            $data['message'] = "fail";
        }

        return response($data);
    }

    public function getLastKilometerVehicle(Request $request)
    {
        $kendaraan = $request->kendaraan;

        $ad = AkhirDinas::where('kendaraan', $kendaraan)->get()->last();
        
        if ($ad['id'] != null) 
        {
            return response(['kilometer' => $ad['kilometer_akhir']]);
        }
        else
        {
            return response(['kilometer' => 0]);
        }
    }
}
