<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Pelapor;
use App\Models\Ljt;
use App\Models\Pjr;

class PelaporController extends Controller
{
    public function create(Request $request)
    {
        $username = $request->username;
        
        $data = array();

        // Cek apakah username sudah ada di database
        if (Pjr::where('nik', $username)->exists() ||
            Ljt::where('nik', $username)->exists() ||
            Pelapor::where('username', $username)->exists()
            ) 
        {
            $data['message'] = "Username sudah ada. Gunakan username yang lain";
        }
        else 
        {
            $pelapor = new Pelapor;
            $pelapor->username = $username;
            $pelapor->password = $request->password;
            $pelapor->nama = $request->nama;
            $pelapor->telepon = $request->telepon;
            $pelapor->email = $request->email;
    
            if ($request->hasFile('foto')) 
            {
                $image       = $request->file('foto');
                $filename    = $username.'.'.$image->extension();

                // resize image
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(700, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                // upload iamge
                $image_resize->save(public_path('images/users/'.$filename));

                $pelapor->foto = $filename;
            }
            
            // saving to database
            if ($pelapor->save()) {
                $data['message'] = "success";
            }
            else {
                $data['message'] = "fail";
            }
        }

        return response($data);
    }

    public function update(Request $request)
    {
        $id = $request->id;

        $pelapor = Pelapor::find($id);
        $pelapor->username = $request->username;
        $pelapor->nama = $request->nama;
        $pelapor->telepon = $request->telepon;
        $pelapor->email = $request->email;
        

        if ($pelapor->save())
            return response(['message' => 'success']);
        else
            return response(['message' => 'fail']);
    }
}
