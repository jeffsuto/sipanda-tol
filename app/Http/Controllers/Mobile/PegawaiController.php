<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pjr;
use App\Models\Ljt;
use App\Models\Laporan;
use App\Models\Dinas;
use App\Models\AkhirDinas;

class PegawaiController extends Controller
{
    /**
     * Check apakah sudah login atau belum
     */
    public function checkLogin(Request $request)
    {
        $nik = $request->nik;

        $dinas = Dinas::where(['nik' => $nik, 'status' => 0])->whereDate('created_at', date('Y-m-d'))->get()->last();
        $ad = AkhirDinas::where('nik', $request->nik)->get()->last();

        if ($dinas['id'] != null) 
        {
            $data['message'] = 'success';
            $data['kendaraan'] = $dinas['kendaraan'];
            $data['status'] = $dinas['status'];
            
            // if ($ad['nik'] != null) {
            //     $data['kilometer_akhir'] = $ad['kilometer_akhir'];
            // }else{
            //     $data['kilometer_akhir'] = 0;
            // }
        }
        else
        {
            $data['message'] = 'fail';
            
            // if ($ad['nik'] != null) {
            //     $data['kilometer_akhir'] = $ad['kilometer_akhir'];
            // }else{
            //     $data['kilometer_akhir'] = 0;
            // }
        }

        return response($data);
    }

    /*
    *   Check status dinas ---------------
    */
    public function checkStatusDinas(Request $request)
    {
        $id = $request->id;
        $role = $request->role;

        $data = array();

        switch ($role) {
            case 'PJR':
                
                $pjr = Pjr::where('id', $id)->first();

                $data['message'] = "success";
                $data['status'] = $pjr->status;

                break;
            
            default:
                
                $ljt = Ljt::where('id', $id)->first();

                $data['message'] = "success";
                $data['status'] = $ljt->status;
        }

        return response($data);
    }

    /*
    *   Untuk menentukan petugas sedang inspeksi atau tidak
    */
    public function updateInspeksi(Request $request)
    {
        $id = $request->id;
        $role = $request->role;
        $status = $request->status;

        $data = array();

        if ($status == "2") {
            $data['status'] = 2;
        }else{
            $data['status'] = 1;
        }

        switch ($role) {
            case 'PJR':
                $pjr = Pjr::find($id);

                $pjr->status = $status;
                
                if ($pjr->save()) {
                    $data['message'] = "success"; 
                }else{
                    $data['message'] = "fail";
                }
                break;
            
            default:
                $ljt = Ljt::find($id);

                $ljt->status = $status;
                
                if ($ljt->save()) {
                    $data['message'] = "success"; 
                }else{
                    $data['message'] = "fail";
                }
                break;
        }

        return response($data);
    }
}
