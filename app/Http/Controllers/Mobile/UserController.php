<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Pjr;
use App\Models\Ljt;
use App\Models\Pelapor;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $data = array();

        $username = $request->username;
        $password = $request->password;

        $pjr = Pjr::where(['nik' => $username,'password' => $password])->get();
        $ljt = Ljt::where(['nik' => $username,'password' => $password])->get();
        $pelapor = Pelapor::where(['username' => $username,'password' => $password])->get();
    
        // Cek PJR
        if( count($pjr) > 0 )
        {
            $pjr = $pjr->first();
            if($pjr->nik == $username && $pjr->password == $password)
            {
                $data['message'] = "success";
                $data['role'] = "PJR";
                $data['id'] = $pjr->id;
            }
        }
        // Cek LJT
        else if( count($ljt) > 0 )
        {
            $ljt = $ljt->first();
            if($ljt->nik == $username && $ljt->password == $password)
            {
                $data['message'] = "success";
                $data['role'] = "LJT";
                $data['id'] = $ljt->id;
            }
        }
        // Cek Pelapor
        else if( count($pelapor) > 0 )
        {
            $pelapor = $pelapor->first();
            if($pelapor->username == $username && $pelapor->password == $password)
            {
                $data['message'] = "success";
                $data['role'] = "pelapor";
                $data['id'] = $pelapor->id;;
            }
        }
        else
        {
            $data['message'] = "fail";
        }

        return response($data);
    }

    public function show(Request $request)
    {
        $id = $request->id;
        $role = $request->role;
        $data = array();
        switch ($role) {
            case 'pelapor':
                $data['data'] = Pelapor::where('id', $id)->get()->first();
                break;
            
            default:
                if ($role == 'PJR') {
                    $data['data'] = Pjr::where('id', $id)->get()->first();
                }else{
                    $data['data'] = Ljt::where('id', $id)->get()->first();
                }
                break;
        }

        return response($data);
    }

    public function checkExistential(Request $request)
    {
        $username = $request->username;
        
        if (Pjr::where('nik', $username)->exists() ||
            Ljt::where('nik', $username)->exists() ||
            Pelapor::where('username', $username)->exists()
            )
        {
            return response(['message' => 'success']);
        } 
        else 
        {
            return response(['message' => 'fail']);
        }
    }

    /**
     * Lupa username
     */
    public function lupaUsername(Request $request)
    {
        $email = $request->email;

        $pelapor = Pelapor::where('email', $email)->get()->first();

        if ($pelapor['id'] != null) 
        {
            $username = $pelapor->username;
            $to_name = $pelapor->nama;
            $to_email = $email;
            $data = array('username'=> $username, "nama" => $to_name);
                
            \Mail::send('emails.lupa-password', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                        ->subject('Lupa Username');
                $message->from('sipanda-tol@ptjpt.co.id','SIPanda Tol');
            });
            $data['message'] = "success";
        }
        else
        {
            $data['message'] = "Email yang anda masukkan tidak terdaftar.";
        }

        return response($data);
    }

    /**
     * Reset password
     */
    public function resetPassword(Request $request)
    {
        $data = array();

        if ($request->filled('password') && $request->filled('repassword')) 
        {
            $pelapor = Pelapor::find($request->id);
            $pelapor->password = $request->password;
            
            if ($pelapor->save()) 
            {
                echo "<script>alert('Password berhasil direset')</script>";
            }
            else
            {
                echo "<script>alert('Gagal mereset password')</script>";
            }
        }
        else
        {
            $pelapor = Pelapor::where('email', $request->email)->get()->first();

            if ($pelapor['id'] != null) 
            {
                $username = $pelapor->username;
                $to_name = $pelapor->nama;
                $to_email = $request->email;
                $data = array('id'=> $pelapor->id);
                    
                $isSend = \Mail::send('emails.reset-password', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('Reset Password');
                    $message->from('sipanda-tol@ptjpt.co.id','SIPanda Tol');
                });
                
                $data['message'] = "success";
            }
            else
            {
                $data['message'] = "Email yang anda masukkan tidak terdaftar.";
            }

            return response($data);
        }
    }

    /**
     * Update foto profile
     */
    public function updateProfile(Request $request)
    {
        $user = "";
        switch ($request->role) {
            case 'LJT':
                $user = Ljt::find($request->id);
                break;
            
            case 'PJR':
                $user = Pjr::find($request->id);
                break;

            default:
                $user = Pelapor::find($request->id);
                $user->username = $request->username;
                $user->nama = $request->nama;
                $user->telepon = $request->telepon;
                $user->email = $request->email;
                break;
        }

        if ($request->hasFile('foto')) 
        {
            $image       = $request->file('foto');
            $filename    = $request->username.'.'.$image->extension();
            
            // resize image
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            // upload iamge
            $image_resize->save(public_path('images/users/'.$filename));

            // menyimpan nama foto ke database
            $user->foto = $filename;
        }

        if ($user->save()) 
        {
            return response(['message' => 'success']);
        }
        else
        {
            return response(['message' => 'fail']);
        }
    }
}
