<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Laporan;

class LaporanInspeksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $laporan_inspeksi = new Laporan;
        $laporan_inspeksi->jenis = $request->jenis;
        $laporan_inspeksi->user = $request->username;
        $laporan_inspeksi->kilometer = $request->kilometer;
        $laporan_inspeksi->arah = $request->arah;
        $laporan_inspeksi->keterangan = $request->keterangan;

        /**
         * jika yang mengirim laporan dari petugasnya langsung
         */
        if ($request->filled('role'))
        {
            if ($request->role != 'pelapor') 
            {
                $laporan_inspeksi->tindak_lanjut = $request->username;
            }
        }

        /**
         * Image processing
         */
        $image = $request->file('foto');
        if($request->hasFile('foto'))
        {
            $filename = time().'.'.$image->extension();

            // resize image
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            // upload iamge
            $image_resize->save(public_path('images/laporan/'.$filename));

            $laporan_inspeksi->foto = $filename;
        }

        if ($laporan_inspeksi->save()) {
            return response( ['message' => 'success'] );
        }else{
            return response( ['message' => 'fail'] );
        }
    }

    /**
     *  Update status akhir laporan
     */
    public function updateStatusAkhir(Request $request)
    {
        $id = $request->id;
        $laporan = Laporan::find($id);
        $laporan->status_akhir = "1";

        if ($laporan->save())
            return response(['message' => 'success']);
        else
            return response(['message' => 'fail']);
    }

    /**
     *  mengambil laporan abru yang harus di tindak lanjuti berdasarkan nik petugas
     */
    public function getLaporanBaru(Request $request)
    {
        $nik = $request->nik;
        $laporans = Laporan::where(['tindak_lanjut' => $nik, 'status_akhir' => "0"])->whereDate('created_at', date('Y-m-d'))
                                ->orderBy('created_at', 'DESC')->get();
        
        $data['data'] = array();

        foreach ($laporans as $laporan) {

            if ($laporan->ljts['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->ljts['nama'],
                    'fotoUser' => $laporan->ljts['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else if ($laporan->pjrs['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->pjrs['nama'],
                    'fotoUser' => $laporan->pjrs['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else if ($laporan->pelapors['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->pelapors['nama'],
                    'fotoUser' => $laporan->pelapors['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else 
            {
                $data['data'][] = [
                    'nama' => 'SENKOM',
                    'fotoUser' => 'user_img.png',
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
        }

        return response($data);
    }

    /**
     *  mengambil laporan yang sudah tervalidasi
     */
    public function getLaporanValidasi(Request $request)
    {
        $nik = $request->nik;
        $laporans = Laporan::where(['tindak_lanjut' => $nik, 'status_akhir' => "1"])
                                ->orderBy('created_at', 'DESC')->get();
        
        $data['data'] = array();

        foreach ($laporans as $laporan) {

            if ($laporan->ljts['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->ljts['nama'],
                    'fotoUser' => $laporan->ljts['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else if ($laporan->pjrs['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->pjrs['nama'],
                    'fotoUser' => $laporan->pjrs['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else if ($laporan->pelapors['id'] != null)
            {
                $data['data'][] = [
                    'nama' => $laporan->pelapors['nama'],
                    'fotoUser' => $laporan->pelapors['foto'],
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
            else 
            {
                $data['data'][] = [
                    'nama' => 'SENKOM',
                    'fotoUser' => 'user_img.png',
                    'id' => $laporan['id'],
                    'user' => $laporan['user'],
                    'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'kilometer' => $laporan['kilometer'],
                    'keterangan' => $laporan['keterangan'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto']
                ];
            }
        }

        return response($data);
    }

    /**
     * mengambil riwayat laporan pelapor
     */
    public function getRiwayatLaporan(Request $request)
    {
        $username = $request->username;
        $laporans = Laporan::where(['user' => $username])->orderBy('created_at', 'DESC')->get();

        $data['data'] = array();
        foreach ($laporans as $laporan) {
            $data['data'][] = [
                'nama' => $laporan->pelapors['nama'],
                'fotoUser' => $laporan->pelapors['foto'],
                'id' => $laporan['id'],
                'user' => $laporan['user'],
                'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                'kilometer' => $laporan['kilometer'],
                'keterangan' => $laporan['keterangan'],
                'arah' => $laporan['arah'],
                'jenis' => $laporan['jenis'],
                'foto' => $laporan['foto'],
                'status_akhir' => $laporan['status_akhir']
            ];
        };
        return response($data);
    }

    /**
     * mengambil data laporan inspeksi berdasarkan id
     */
    public function getLaporanInspeksi($id)
    {
        $data = Laporan::where('id', $id)->get();
        return response($data);
    }
}
