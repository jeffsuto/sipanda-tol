<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Info;
use App\Models\Dinas;

class InfoLaluLintasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $action = null)
    {
        $data['title'] = 'Info Lalu Lintas';
        $data['pageActive'] = 'active';
        $data['info'] = array();
        $data['dinas'] = array();
        $data['tanggal'] = "Semua";
        $data['shift'] = "Semua";
        
        if ($request->filled('tanggal') && $request->filled('shift')) 
        {
            $date = $request->tanggal;
            
            $from = "";
            $until = "";
            $jenis = $request->jenis;

            switch ($request->shift) {
                case '1':
                    $from = $date.' '.date('06:00:00');
                    $until = $date.' '.date('13:59:59');
                    break;
                case '2':
                    $from = $date.' '.date('14:00:00');
                    $until = $date.' '.date('20:59:59');
                    break;
                default:
                    $from = $date.' '.date('21:00:00');
                    $until = date('Y-m-d', strtotime($from.' + 1 days') ).' '.date('05:59:59');
                    break;
            }

            $data['tanggal'] = $request->tanggal;
            $data['shift'] = $request->shift;

            /**
             * Karyawan yang bertugas
             */
            $dinas = Dinas::where('status', '1')->whereBetween('created_at', [$from, $until])->get();
            $no = 0;
            foreach ($dinas as $d) 
            {
                if ($d->ljts['id'] != null) 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->ljts['nama'],
                        'kendaraan' => $d['kendaraan']
                    ]; 
                }
                else 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->pjrs['nama'],
                        'kendaraan' => $d['kendaraan']
                    ];
                }
            }

            /**
             * Info lalu lintas
             */
            if ($data['shift'] == 0) {
                $infos = Info::whereDate('created_at', $date)->get();
            }else{
                $infos = Info::whereBetween('created_at', [$from, $until])->get();
            }
            $no = 0;
            foreach ($infos as $info) 
            {
                $no++;
                $data['info'][] = [
                    'no' => $no,
                    'waktu' => $info->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $info['keterangan']
                ];
            }
        }

        if ($action != null) 
        {
            $data['date_now'] = date('d F Y');
            return view('print.pages.info-lalu-lintas.content', $data);
        }
        else
        {
            return view('pages.info-lalu-lintas.index', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $info = new Info;
        $info->keterangan = $request->keterangan;

        $info->save();

        return redirect()->route('halaman-utama');
    }

    public function showInfoLaluLintas(Request $request)
    {
        $data['message'] = "success";
        $data['data'] = Info::whereDate('created_at', date('Y-m-d'))->where('status', '1')->orderBy('created_at', 'DESC')->get();

        return response($data);
    }

    public function showInfoLaluLintasById($id)
    {
        return response( Info::where('id', $id)->get() );
    }

    public function update(Request $request)
    {
        $info = Info::find($request->id);
        $info->keterangan = $request->keterangan;
        $info->save();

        return redirect()->route('halaman-utama');
    }

    public function activateInfo($id)
    {
        $info = Info::find($id);
        $info->status = '1';
        $info->save();

        return redirect()->route('halaman-utama');
    }

    public function deactivateInfo($id)
    {
        $info = Info::find($id);
        $info->status = '0';
        $info->save();

        return redirect()->route('halaman-utama');
    }
}