<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\AkhirDinas;
use App\Models\Ljt;
use App\Models\Pjr;

class RekapLaporanController extends Controller
{
    public function index(Request $request, $action = null)
    {
        
        $kejadian = array();
        $ljt = array();
        $pjr = array();
        $data['dinas'] = array();
        $data['tanggal'] = "Semua";
        $data['shift'] = "Semua";

        if ($request->filled('tanggal_dari') && $request->filled('tanggal_sampai')) 
        {
            $from = $request->tanggal_dari;
            $until = $request->tanggal_sampai;
            $petugas = $request->petugas;
            
            $filter['nik'] = $request->nama;
            $filter['jabatan'] = $request->jabatan;

            $data['tanggal'] = date('d-m-Y', strtotime($from)).' s/d '.date('d-m-Y', strtotime($until));

            switch ($petugas) {
                case '0':
                    if ($filter['jabatan'] != '4' && $filter['jabatan'] != '0') 
                        $ljt = $this->getDataLjt($from, $until, $filter);
                    elseif ($filter['jabatan'] == '4') 
                        $pjr = $this->getDataPjr($from, $until, $filter);
                    else {
                        $ljt = $this->getDataLjt($from, $until, $filter);
                        $pjr = $this->getDataPjr($from, $until, $filter);
                    }
                    break;
                
                default:
                    /**
                     * menghitung kejadian
                     */
                    switch ($petugas) {
                        case 'LJT':
                            $ljt = $this->getDataLjt($from, $until, $filter);
                            break;
                        
                        default:
                            $pjr = $this->getDataPjr($from, $until, $filter);
                            break;
                    }
                    break;
            }

            foreach ($ljt as $l) {

                if ($l->nik != null) {
    
                    // kilometer awal dan akhir kendaraan 217
                    $km_awal_217 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 1])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_217 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 1])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
                    
                    // kilometer awal dan akhir kendaraan 218
                    $km_awal_218 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 2])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_218 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 2])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan rescue
                    $km_awal_rescue = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 3])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_rescue = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 3])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan ambulan
                    $km_awal_ambulan = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 4])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_ambulan = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 4])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan pjr 211
                    $km_awal_211 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 5])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_211 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 5])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');

                    // kilometer awal dan akhir kendaraan pjr 212
                    $km_awal_212 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 6])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_212 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 6])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');

                    $kejadian[] = [
                        'nik' => $l->nik,
                        'nama' => $l->nama,
                        'jabatan' => $l->jabatan,
                        'mogok' => $l->mogok,
                        'kecelakaan' => $l->kecelakaan,
                        'kerusakan_jalan' => $l->kerusakan_jalan,
                        'kebakaran' => $l->kebakaran,
                        'kemacetan' => $l->kemacetan,
                        'lain' => $l->lain,
                        'patroli_217' => abs($km_akhir_217 - $km_awal_217),
                        'patroli_218' => abs($km_akhir_218 - $km_awal_218),
                        'rescue' => abs($km_akhir_rescue - $km_awal_rescue),
                        'ambulan' => abs($km_akhir_ambulan - $km_awal_ambulan),
                        'pjr_211' => abs($km_akhir_211 - $km_awal_211),
                        'pjr_212' => abs($km_akhir_212 - $km_awal_212)
                    ];
                }
            }
            
            foreach ($pjr as $l) {
                if ($l->nik != null) {
                    // kilometer awal dan akhir kendaraan 217
                    $km_awal_217 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 1])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_217 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 1])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan 218
                    $km_awal_218 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 2])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_218 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 2])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan rescue
                    $km_awal_rescue = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 3])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_rescue = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 3])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
    
                    // kilometer awal dan akhir kendaraan ambulan
                    $km_awal_ambulan = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 4])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_ambulan = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 4])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');
                    
                    // kilometer awal dan akhir kendaraan pjr 211
                    $km_awal_211 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 5])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_211 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 5])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');

                    // kilometer awal dan akhir kendaraan pjr 212
                    $km_awal_212 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 6])->whereBetween('created_at', [$from, $until])->sum('kilometer_awal');
                    $km_akhir_212 = AkhirDinas::where(['nik' => $l->nik, 'kendaraan' => 6])->whereBetween('created_at', [$from, $until])->sum('kilometer_akhir');

                    $kejadian[] = [
                        'nik' => $l->nik,
                        'nama' => $l->nama,
                        'jabatan' => 4,
                        'mogok' => $l->mogok,
                        'kecelakaan' => $l->kecelakaan,
                        'kerusakan_jalan' => $l->kerusakan_jalan,
                        'kebakaran' => $l->kebakaran,
                        'kemacetan' => $l->kemacetan,
                        'lain' => $l->lain,
                        'patroli_217' => abs($km_akhir_217 - $km_awal_217),
                        'patroli_218' => abs($km_akhir_218 - $km_awal_218),
                        'rescue' => abs($km_akhir_rescue - $km_awal_rescue),
                        'ambulan' => abs($km_akhir_ambulan - $km_awal_ambulan),
                        'pjr_211' => abs($km_akhir_211 - $km_awal_211),
                        'pjr_212' => abs($km_akhir_212 - $km_awal_212)
                    ];
                }
            }
        }
        
        $data['title'] = "Rekap Laporan Kerja";
        $data['rekaps'] = $kejadian;
        $data['pjrs'] = Pjr::all();
        $data['ljts'] = Ljt::all();
        
        if ($action != null)
        {
            $data['date_now'] = date('d F Y');
            $data['rekap'] = true;
            return view('print.pages.rekap-laporan.content', $data);
        }
        else
        {
            return view('pages.rekap-laporan.index', $data);
        }
    }

    /**
     * Get data LJT
     */
    public function getDataLjt($from, $until, $filter = array())
    {
        $condition = "";
        $nik = $filter['nik'];
        $jabatan = $filter['jabatan'];

        if ($jabatan != '0' && $nik != '0') 
            $condition = " WHERE ljts.jabatan = $jabatan AND tindak_lanjut = $nik AND date(laporans.created_at) between '$from' and '$until'";
        elseif ($jabatan != '0' && $nik == '0')
            $condition = " WHERE ljts.jabatan = $jabatan AND date(laporans.created_at) between '$from' and '$until'";
        elseif ($jabatan == '0' && $nik != '0')
            $condition = " WHERE tindak_lanjut = $nik AND date(laporans.created_at) between '$from' and '$until'";
        else
            $condition = " WHERE date(laporans.created_at) between '$from' and '$until' ";

        $ljt = DB::select(
            "SELECT 
            DISTINCT tindak_lanjut AS nik,
            ljts.nama,
            ljts.jabatan,
            COUNT(IF(jenis=1, 1, NULL)) AS mogok,
            COUNT(IF(jenis=2, 1, NULL)) AS kecelakaan,
            COUNT(IF(jenis=3, 1, NULL)) AS kerusakan_jalan,
            COUNT(IF(jenis=4, 1, NULL)) AS kebakaran,
            COUNT(IF(jenis=5, 1, NULL)) AS kemacetan,
            COUNT(IF(jenis=6, 1, NULL)) AS lain
            FROM 
                laporans
            RIGHT JOIN 
                ljts ON laporans.tindak_lanjut = ljts.nik
            $condition
            GROUP BY 
                tindak_lanjut
            ORDER BY 
                ljts.nik"
        );

        return $ljt;
    }

    /**
     * Get data PJR
     */
    public function getDataPjr($from, $until, $filter = array())
    {
        $condition = "";
        $nik = $filter['nik'];
        $jabatan = $filter['jabatan'];

        if ($nik != '0')
            $condition = " WHERE tindak_lanjut = $nik AND date(laporans.created_at) between '$from' and '$until'";
        else
            $condition = " WHERE date(laporans.created_at) between '$from' and '$until' ";

        $pjr = DB::select(
            "SELECT 
            DISTINCT tindak_lanjut AS nik,
            pjrs.nama,
            COUNT(IF(jenis=1, 1, NULL)) AS mogok,
            COUNT(IF(jenis=2, 1, NULL)) AS kecelakaan,
            COUNT(IF(jenis=3, 1, NULL)) AS kerusakan_jalan,
            COUNT(IF(jenis=4, 1, NULL)) AS kebakaran,
            COUNT(IF(jenis=5, 1, NULL)) AS kemacetan,
            COUNT(IF(jenis=6, 1, NULL)) AS lain
            FROM 
                laporans
            RIGHT JOIN 
                pjrs ON laporans.tindak_lanjut = pjrs.nik
            $condition
            GROUP BY 
                tindak_lanjut
            ORDER BY 
                pjrs.nik"
        );

        return $pjr;
    }
}
