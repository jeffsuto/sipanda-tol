<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Laporan;
use App\Models\Dinas;

class LaporanPelaporController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $action = null)
    {
        $data['title'] = 'Laporan Pelapor';

        $data['laporans'] = array();
        $data['dinas'] = array();
        $data['tanggal'] = "Semua";
        $data['shift'] = "Semua";
        
        if ($request->filled('tanggal') && $request->filled('shift') && $request->filled('jenis')) 
        {
            $date = $request->tanggal;
            
            $from = "";
            $until = "";
            $jenis = $request->jenis;

            switch ($request->shift) {
                case '1':
                    $from = $date.' '.date('06:00:00');
                    $until = $date.' '.date('13:59:59');
                    break;
                case '2':
                    $from = $date.' '.date('14:00:00');
                    $until = $date.' '.date('20:59:59');
                    break;
                default:
                    $from = $date.' '.date('21:00:00');
                    $until = date('Y-m-d', strtotime($from.' + 1 days') ).' '.date('05:59:59');
                    break;
            }
            
            $data['tanggal'] = $request->tanggal;
            $data['shift'] = $request->shift;

            /**
             * Karyawan yang bertugas
             */
            $dinas = Dinas::where('status', '1')->whereBetween('created_at', [$from, $until])->get();
            $no = 0;
            foreach ($dinas as $d) 
            {
                if ($d->ljts['id'] != null) 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->ljts['nama'],
                        'kendaraan' => $d['kendaraan']
                    ]; 
                }
                else 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->pjrs['nama'],
                        'kendaraan' => $d['kendaraan']
                    ];
                }
            }

            /**
             * Laporan pelapor
             */
            $laporans = "";
            if ($request->shift == 0 && $request->jenis == 0) {

                $laporans = Laporan::whereDate('created_at', $date)->get();

            }elseif ($request->shift != 0 && $request->jenis == 0) {
                
                $laporans = Laporan::whereBetween('created_at', [$from, $until])->get();

            }elseif ($request->shift == 0 && $request->jenis != 0) {

                $laporans = Laporan::where('jenis', $jenis)->whereDate('created_at', $date)->get();

            }else{

                $laporans = Laporan::where('jenis', $jenis)->whereDate('created_at', $date)->get();

            }
            
            $no = 0;
            foreach ($laporans as $laporan) {
                
                if ($laporan->pelapors['id'] != null)
                {
                    $no++;
                    $data['laporans'][] = [
                        'no' => $no,
                        'id' => $laporan['id'],
                        'laporan' => $laporan->pelapors['nama'],
                        'waktu' => $laporan->created_at->format('d-m-Y H:i:s'),
                        'kilometer' => $laporan['kilometer'],
                        'keterangan' => $laporan['keterangan'],
                        'arah' => $laporan['arah'],
                        'tindak_lanjut' => $laporan['tindak_lanjut'],
                        'status_akhir' => $laporan['status_akhir'],
                        'jenis' => $laporan['jenis'],
                        'foto' => $laporan['foto']
                    ];
                }
            }
        }
        
        if ($action != null) 
        {   
            $data['date_now'] = date('d F Y');
            return view('print.pages.laporan-pengguna-jalan.content', $data);
        }
        else
        {
            return view('pages.laporan-pelapor.index', $data);
        }
    }
}
