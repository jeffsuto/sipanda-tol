<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Senkom;

class LoginController extends Controller
{
    public function checkLogin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $cek = Senkom::where([
            'nik' => $username,
            'password' => $password
        ])->get();
      
        if (count($cek) > 0) {
            session(['IS_LOGIN' => TRUE]);
            return redirect()->route('halaman-utama');
        }else{
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        session()->flush();
        return redirect()->route('login');
    }
}
