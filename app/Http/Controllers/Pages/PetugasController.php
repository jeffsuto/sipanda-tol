<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Ljt;
use App\Models\Pjr;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Petugas';
        $data['pageActive'] = 'active';

        $data['pjr'] = Pjr::all();
        $data['ljt'] = Ljt::all();

        return view('pages.petugas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $role = $request->petugas;
        $nik = $request->nik;
        
        switch ($role) {
            case 'LJT':
                $ljt = new Ljt;

                $ljt->nik       = $nik;
                $ljt->password  = $request->password;
                $ljt->nama      = $request->nama;
                $ljt->jabatan   = $request->jabatan;

                if ($request->hasFile('foto')) {
                    
                    $image       = $request->file('foto');
                    $filename    = $nik.'.'.$image->extension();

                    // resize image
                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    // upload iamge
                    $image_resize->save(public_path('images/users/'.$filename));

                    $ljt->foto = $filename;
                }

                $ljt->save();

                return redirect()->route('petugas');

                break;
            
            default:
                
                $pjr = new Pjr;

                $pjr->nik       = $request->nik;
                $pjr->password  = $request->password;
                $pjr->nama      = $request->nama;
                
                if ($request->hasFile('foto')) {
                    
                    $image       = $request->file('foto');
                    $filename    = $nik.'.'.$image->extension();

                    // resize image
                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    // upload iamge
                    $image_resize->save(public_path('images/users/'.$filename));

                    $pjr->foto = $filename;

                }

                $pjr->save();

                return redirect()->route('petugas');


                break;
        }
    }
    
    /**
     * API data petugas individu
     */
    public function getDataPetugas($id, $role)
    {
        switch ($role) {
            case 'PJR':
                return Pjr::where('id', $id)->get();
                break;
            
            default:
                return Ljt::where('id', $id)->get();
                break;
        }
    }

    /**
     * Update petugas
     */
    public function update(Request $request)
    {
        $role = $request->petugas;
        $nik = $request->nik;
        $id = $request->id;
        
        switch ($role) {
            case 'LJT':
                $ljt = Ljt::find($id);

                $ljt->nik       = $nik;
                $ljt->password  = $request->password;
                $ljt->nama      = $request->nama;
                $ljt->jabatan   = $request->jabatan;

                if ($request->hasFile('foto')) {
                    
                    $image       = $request->file('foto');
                    $filename    = $nik.'.'.$image->extension();

                    // resize image
                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    // upload iamge
                    $image_resize->save(public_path('images/users/'.$filename));

                    $ljt->foto = $filename;
                }

                $ljt->save();

                return redirect()->route('petugas');

                break;
            
            default:
                
                $pjr = Pjr::find($id);

                $pjr->nik       = $request->nik;
                $pjr->password  = $request->password;
                $pjr->nama      = $request->nama;
                
                if ($request->hasFile('foto')) {
                    
                    $image       = $request->file('foto');
                    $filename    = $nik.'.'.$image->extension();

                    // resize image
                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    // upload iamge
                    $image_resize->save(public_path('images/users/'.$filename));

                    $pjr->foto = $filename;

                }

                $pjr->save();

                return redirect()->route('petugas');


                break;
        }
    }

    /**
     * Delete petugas
     */
    public function delete(Request $request, $role)
    {
        $id = $request->id;

        switch ($role) {
            case 'PJR':
                Pjr::find($id)->delete();
                break;
            
            default:
                Ljt::find($id)->delete();
                break;
        }

        return redirect()->route('petugas');
    }
}
