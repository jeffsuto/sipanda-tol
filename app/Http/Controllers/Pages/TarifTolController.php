<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Tarif;

class TarifTolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Tarif Tol';
        $data['pageActive'] = 'active';
        $data['image'] = array();
        $data['image'] = Tarif::latest()->first();
        
        return view('pages.tarif-tol.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('foto');
        
        $name = "tarif-tol".".".$image->extension();

        $image->storeAs('public/images/tarif', $name);

        $tarif = new Tarif;
        $tarif->foto = $name;

        $tarif->save();

        return redirect()->route('tarif-tol');
    }
}
