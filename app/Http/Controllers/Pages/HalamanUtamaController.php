<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;

use App\Models\Pjr;
use App\Models\Ljt;
use App\Models\Info;
use App\Models\Dinas;
use App\Models\Laporan;

class HalamanUtamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Halaman Utama';
        $data['pageActive'] = 'active';
        $data['info'] = Info::whereDate('created_at', date('Y-m-d'))->get();
        $data['pegawai'] = array();
        
        $last_id = Info::all()->last();
        
        if ($last_id != null) {
            $data['next_id'] = $last_id['id']+1;
        }else{
            $data['next_id'] = 1;
        }

        /**
         * Karyawan yang bertugas
         */
        $datenow = date('Y-m-d');
        $timenow = date('H:i:s');

        $ljts = "";
        $pjrs = "";
        $dinas = "";
        /**
         * Shift karyawan
         */
        if ($timenow >= date('06:00:00') && $timenow < date('14:00:00')) 
        {
            $dinas = $this->getDinasKaryawan($datenow, '1');
        }
        elseif ($timenow >= date('14:00:00') && $timenow < date('21:00:00')) 
        {
            $dinas = $this->getDinasKaryawan($datenow, '2');
        }
        else
        {
            $dinas = $this->getDinasKaryawan($datenow, '3');
        }

        $ljts = $dinas['ljts'];
        $pjrs = $dinas['pjrs'];
        /**
         * Mengambil data sesuai dengan jadwal shift sekarang
         */
        foreach ($ljts as $l) {
            $data['pegawai'][] = [
                'id' => $l->id,
                'nik' => $l->nik,
                'nama' => $l->nama,
                'kendaraan' => $l->kendaraan,
                'jabatan' => $l->jabatan,
                'status' => $l->status,
                'status_dinas' => $l->status_dinas
            ];
        }
        foreach ($pjrs as $l) {
            $data['pegawai'][] = [
                'id' => $l->id,
                'nik' => $l->nik,
                'nama' => $l->nama,
                'kendaraan' => $l->kendaraan,
                'jabatan' => '4',
                'status' => $l->status,
                'status_dinas' =>$l->status_dinas
            ];
        }
        
        /**
         * Rangkaian laporan kegiatan
         */
        $laporans = Laporan::whereDate('created_at', date('Y-m-d'))->get();
        $data['laporan'] = array();
        $no_laporan = 0;
        foreach ($laporans as $laporan) 
        {
            $no_laporan++;
            if ($laporan->ljts['id'] != null) 
            {
                $data['laporan'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->ljts['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else if ($laporan->pjrs['id'] != null) 
            {
                $data['laporan'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->pjrs['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else if ($laporan->pelapors['id'] != null) 
            {
                $data['laporan'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->pelapors['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else
            {
                $data['laporan'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => 'SENKOM',
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
        }
        $data['next_laporan'] = $no_laporan + 1;
        return view('pages.halaman-utama.index', $data);
    }

    /**
     * Menambahkan laporan inspeksi
     */
    public function createLaporanInspeksi(Request $request)
    {
        $laporan_inspeksi = new Laporan;

        $laporan_inspeksi->jenis = $request->jenis;
        $laporan_inspeksi->user = $request->username;
        $laporan_inspeksi->kilometer = $request->kilometer;
        $laporan_inspeksi->arah = $request->arah;
        $laporan_inspeksi->keterangan = $request->keterangan;

        if ($request->has('tindak_lanjut')){
            $laporan_inspeksi->tindak_lanjut = $request->tindak_lanjut;
        }

        $image = $request->file('foto');

        if($request->hasFile('foto'))
        {
            $filename = time().'.'.$image->extension();

            // resize image
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            // upload iamge
            $image_resize->save(public_path('images/laporan/'.$filename));

            $laporan_inspeksi->foto = $filename;
        }

        $laporan_inspeksi->save();

        return redirect()->route('halaman-utama');
    }

    /**
     * Update Tindak lanjut laporan
     */
    public function updateTindakLanjut(Request $request)
    {
        $id = $request->id;
        
        $laporan = Laporan::find($id);
        $laporan->tindak_lanjut = $request->tindak_lanjut;
        $laporan->save();
        
        return redirect()->route('halaman-utama');
    }

    /**
     * Logout petugas
     */
    public function logoutPegawai($id, $nik, $role)
    {
        switch ($role) {
            case '4':
                Pjr::where('nik', $nik)->update(['status' => 0, 'logout_session' => 0]);
                break;
            
            default:
                Ljt::where('nik', $nik)->update(['status' => 0, 'logout_session' => 0]);
                break;
        }
        
        $dinas = Dinas::find($id);
        $dinas->status = 1;
        $dinas->save();

        return redirect()->route('halaman-utama');
    }

    /*
    *   Update status dinas pegawai menjadi aktif
    */
    public function activateStatusPegawai($nik, $role)
    {
        switch ($role) {
            case '4':
                Pjr::where('nik', $nik)->update(['status' => 1]);
                break;
            
            default:
                Ljt::where('nik', $nik)->update(['status' => 1]);
                break;
        }

        return redirect()->route('halaman-utama');
    }

    /*
    *   Update status dinas pegawai menjadi nonaktif
    */
    public function deactivateStatusPegawai($nik, $role)
    {
        switch ($role) {
            case '4':
                Pjr::where('nik', $nik)->update(['status' => 0]);
                Dinas::where(['nik' => $nik, 'status' => 0])->delete();
                break;
            
            default:
                Ljt::where('nik', $nik)->update(['status' => 0]);
                Dinas::where(['nik' => $nik, 'status' => 0])->delete();
                break;
        }

        return redirect()->route('halaman-utama');
    }

    /**
     * Delete laporan inspeksi
     */
    public function deleteLaporanInspeksi(Request $request)
    {
        Laporan::find($request->id)->delete();
        return redirect()->route('halaman-utama');
    }

    /**
     * Delete info lantas
     */
    public function deleteInfo($id)
    {
        Info::find($id)->delete();
        return redirect()->route('halaman-utama');
    }
    
    /**
     * Update laporan inspeksi
     */
    public function updateLaporanInspeksi(Request $request)
    {
        $laporan = Laporan::find($request->id);
        $laporan->jenis = $request->jenis; 
        $laporan->kilometer = $request->kilometer; 
        $laporan->arah = $request->arah; 
        $laporan->keterangan = $request->keterangan; 
        if ($request->filled('itindak_lanjut'))
            $laporan->tindak_lanjut = $request->itindak_lanjut;
        else
            $laporan->tindak_lanjut = $request->tindak_lanjut;

        $laporan->save();

        return redirect()->route('halaman-utama');
    }

    public function getKaryawanYangBertugas()
    {
        /**
         * Karyawan yang bertugas
         */
        $datenow = date('Y-m-d');
        $timenow = date('H:i:s');

        $Dinas = "";
        $data = array();
        $data['data'] = array();
        /**
         * Shift karyawan
         */
        if ($timenow >= date('06:00:00') && $timenow < date('14:00:00')) 
        {
            $dinas = $this->getDinasKaryawan($datenow, '1');
        }
        elseif ($timenow >= date('14:00:00') && $timenow < date('21:00:00')) 
        {
            $dinas = $this->getDinasKaryawan($datenow, '2');
        }
        else
        {
            $dinas = $this->getDinasKaryawan($datenow, '3');
        }

        $ljts = $dinas['ljts'];
        $pjrs = $dinas['pjrs'];
        /**
         * Mengambil data sesuai dengan jadwal shift sekarang
         */
        foreach ($ljts as $l) {
            $data['data'][] = [
                'id' => $l->id,
                'nik' => $l->nik,
                'nama' => $l->nama,
                'kendaraan' => $l->kendaraan,
                'jabatan' => $l->jabatan,
                'status' => $l->status,
                'status_dinas' => $l->status_dinas,
                'logout_session' => $l->logout_session
            ];
        }
        foreach ($pjrs as $l) {
            $data['data'][] = [
                'id' => $l->id,
                'nik' => $l->nik,
                'nama' => $l->nama,
                'kendaraan' => $l->kendaraan,
                'jabatan' => '4',
                'status' => $l->status,
                'status_dinas' =>$l->status_dinas,
                'logout_session' => $l->logout_session
            ];
        }
        
        return datatables()->of($data['data'])->toJson();
    }

    public function getRangkaianLaporan()
    {
        // $datenow = date('Y-m-d H:i:s');
        // $from = "";
        // $until = "";
        
        // if ($datenow >= date('Y-m-d').' 06:00:00' && $datenow <= ' 14:00:00') {
        //     $from = date('Y-m-d').' 06:00:00';
        //     $until = date('Y-m-d').' 14:00:00';

        //     $laporans = Laporan::whereDate('created_at', [$from, $until])->get();
        // }
        // elseif ($datenow >= date('Y-m-d').' 14:00:00' && $datenow <= ' 21:00:00') {
        //     $from = date('Y-m-d').' 14:00:00';
        //     $until = date('Y-m-d').' 21:00:00';

        //     $laporans = Laporan::whereDate('created_at', [$from, $until])->get();
        // }
        // elseif ($datenow >= date('Y-m-d').' 21:00:00' && $datenow <= ' 23:59:59' ||
        //         $datenow >= date('Y-m-d').' 00:00:00' && $datenow <= ' 06:00:00') 
        // {
        //     $from = date('Y-m-d').' 14:00:00';
        //     $until = date('Y-m-d').' 21:00:00';

        //     $laporans = Laporan::whereDate('created_at', [$from, $until])->get();
        // }
        /**
         * Rangkaian laporan kegiatan
         */
        $laporans = Laporan::whereDate('created_at', date('Y-m-d'))->get();
        $data['data'] = array();
        $no_laporan = 0;
        foreach ($laporans as $laporan) 
        {
            $no_laporan++;
            if ($laporan->ljts['id'] != null) 
            {
                $data['data'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->ljts['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else if ($laporan->pjrs['id'] != null) 
            {
                $data['data'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->pjrs['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else if ($laporan->pelapors['id'] != null) 
            {
                $data['data'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => $laporan->pelapors['nama'],
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
            else
            {
                $data['data'][] = [
                    'no' => $no_laporan,
                    'id' => $laporan['id'],
                    'nama' => 'SENKOM',
                    'created_at' => $laporan->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $laporan['keterangan'],
                    'kilometer' => $laporan['kilometer'],
                    'arah' => $laporan['arah'],
                    'jenis' => $laporan['jenis'],
                    'foto' => $laporan['foto'],
                    'status_akhir' => $laporan['status_akhir'],
                    'tindak_lanjut' => $laporan['tindak_lanjut']
                ];
            }
        }

        return datatables()->of($data['data'])->toJson();
    }

    public function getDinasKaryawan($datenow = "", $shift = "")
    {
        $ljts = DB::select(
            "SELECT 
                dinas.id, dinas.nik, ljts.nama,
                dinas.kendaraan, ljts.jabatan,
                ljts.status, dinas.status AS status_dinas,
                logout_session
            FROM 
                dinas 
            INNER JOIN 
                ljts ON dinas.nik = ljts.nik 
            WHERE 
                (shift = '$shift' OR ljts.status = 1) AND dinas.status = 0 AND DATE(dinas.created_at) = '$datenow'"
        );

        $pjrs = DB::select(
            "SELECT 
                dinas.id, dinas.nik, pjrs.nama,
                dinas.kendaraan, logout_session,
                pjrs.status, dinas.status AS status_dinas
            FROM 
                dinas 
            INNER JOIN 
            pjrs ON dinas.nik = pjrs.nik 
            WHERE 
                (shift = '$shift' OR pjrs.status = 1) AND dinas.status = 0 AND DATE(dinas.created_at) = '$datenow'"
        );

        $data['ljts'] = $ljts;
        $data['pjrs'] = $pjrs;

        return $data;
    }
}
