<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pjr;
use App\Models\Ljt;
use App\Models\Info;
use App\Models\Laporan;
use App\Models\Dinas;
use App\Models\AkhirDinas;

class LaporanKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $action = null)
    {
        $data['title'] = 'Laporan Kerja';
        $data['pageActive'] = 'active';
        
        $data['dinas'] = array();
        $data['info'] = array();
        $data['laporan_kegiatan'] = array();
        $data['kendaraan'] = array();
        $data['tanggal'] = "Semua";
        $data['shift'] = "Semua";
        $data['dinas'] = array();

        if ($request->filled('tanggal') && $request->filled('shift')) 
        {
            $date = $request->tanggal;
            
            $from = "";
            $until = "";

            switch ($request->shift) {
                case '1':
                    $from = $date.' '.date('06:00:00');
                    $until = $date.' '.date('13:59:59');
                    break;
                case '2':
                    $from = $date.' '.date('14:00:00');
                    $until = $date.' '.date('20:59:59');
                    break;
                default:
                    $from = $date.' '.date('21:00:00');
                    $until = date('Y-m-d', strtotime($from.' + 1 days') ).' '.date('05:59:59');
                    break;
            }
            
            $data['tanggal'] = $request->tanggal;
            $data['shift'] = $request->shift;
            
            /**
             * Karyawan yang bertugas
             */
            $dinas = Dinas::where('status', '1')->whereBetween('created_at', [$from, $until])->get();
            $no = 0;
            foreach ($dinas as $d) 
            {
                if ($d->ljts['id'] != null) 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->ljts['nama'],
                        'kendaraan' => $d['kendaraan'],
                        'jabatan' => $d->ljts['jabatan']
                    ]; 
                }
                else 
                {
                    $no++;
                    $data['dinas'][] = [
                        'no' => $no,
                        'nik' => $d['nik'],
                        'nama' => $d->pjrs['nama'],
                        'kendaraan' => $d['kendaraan'],
                        'jabatan' => 'PJR'
                    ];
                }
            }

            // Informasi lalu lintas
            $infos = Info::whereBetween('created_at', [$from, $until])->get();

            $no = 0;
            foreach ($infos as $info) 
            {
                $no++;
                $data['info'][] = [
                    'no' => $no,
                    'waktu' => $info->created_at->format('d-m-Y H:i:s'),
                    'keterangan' => $info['keterangan']
                ];
            }

            // rangkaian laporan kegiatan
            $laporans = Laporan::whereBetween('created_at', [$from, $until])->get();
            $no = 0;
            foreach ($laporans as $l) 
            {
                $no++;
                if ($l->ljts['id'] != null) 
                {
                    $data['laporan_kegiatan'][] = [
                        'nomer' =>$no,
                        'pukul' => $l['created_at']->format('d-m-Y H:i:s'),
                        'laporan' => $l->ljts['nama'],
                        'jenis' => $l['jenis'],
                        'keterangan' => $l['keterangan'],
                        'km' => $l['kilometer'],
                        'arah' => $l['arah'],
                        'foto' => $l['foto'],
                        'tindak_lanjut' => $l['tindak_lanjut'],
                        'status_akhir' => $l['status_akhir']
                    ]; 
                }
                else if ($l->pjrs['id'] != null) 
                {
                    $data['laporan_kegiatan'][] = [
                        'nomer' =>$no,
                        'pukul' => $l['created_at']->format('d-m-Y H:i:s'),
                        'laporan' => $l->pjrs['nama'],
                        'jenis' => $l['jenis'],
                        'keterangan' => $l['keterangan'],
                        'km' => $l['kilometer'],
                        'arah' => $l['arah'],
                        'foto' => $l['foto'],
                        'tindak_lanjut' => $l['tindak_lanjut'],
                        'status_akhir' => $l['status_akhir']
                    ]; 
                }
                else if ($l->pelapors['id'] != null) 
                {
                    $data['laporan_kegiatan'][] = [
                        'nomer' =>$no,
                        'pukul' => $l['created_at']->format('d-m-Y H:i:s'),
                        'laporan' => $l->pelapors['nama'],
                        'jenis' => $l['jenis'],
                        'keterangan' => $l['keterangan'],
                        'km' => $l['kilometer'],
                        'arah' => $l['arah'],
                        'foto' => $l['foto'],
                        'tindak_lanjut' => $l['tindak_lanjut'],
                        'status_akhir' => $l['status_akhir']
                    ]; 
                }
                else
                {
                    $data['laporan_kegiatan'][] = [
                        'nomer' =>$no,
                        'pukul' => $l['created_at']->format('d-m-Y H:i:s'),
                        'laporan' => 'SENKOM',
                        'jenis' => $l['jenis'],
                        'keterangan' => $l['keterangan'],
                        'km' => $l['kilometer'],
                        'arah' => $l['arah'],
                        'foto' => $l['foto'],
                        'tindak_lanjut' => $l['tindak_lanjut'],
                        'status_akhir' => $l['status_akhir']
                    ];
                } 
            }

            // kondisi kendaraan saat akhir dinas
            $kendaraan = AkhirDinas::whereBetween('created_at', [$from, $until])->get();
            $data['kendaraan'] = array();
            foreach ($kendaraan as $k) 
            {
                $km_jumlah = abs($k['kilometer_akhir'] - $k['kilometer_awal']);
                if ($k->ljts['id'] != null) 
                {
                    $data['kendaraan'][] = [
                        'kendaraan' => $k['kendaraan'],
                        'nik' => $k['nik'],
                        'nama' => $k->ljts['nama'],
                        'km_awal' => $k['kilometer_awal'],
                        'km_akhir' => $k['kilometer_akhir'],
                        'km_jumlah' => $km_jumlah,
                        'kondisi' => $k['kondisi'],
                        'keterangan' => $k['keterangan']
                    ];
                }
                else
                {
                    $data['kendaraan'][] = [
                        'kendaraan' => $k['kendaraan'],
                        'nik' => $k['nik'],
                        'nama' => $k->pjrs['nama'],
                        'km_awal' => $k['kilometer_awal'],
                        'km_akhir' => $k['kilometer_akhir'],
                        'km_jumlah' => $km_jumlah,
                        'kondisi' => $k['kondisi'],
                        'keterangan' => $k['keterangan']
                    ];
                }
            }
        }

        if ($action != null) 
        {
            $data['date_now'] = date('d F Y');
            return view('print.pages.laporan-kerja.content', $data);
        }
        else
        {
            return view('pages.laporan-kerja.index', $data);
        }
    }
}
