<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotUsername extends Mailable
{
    use Queueable, SerializesModels;

    public $forgot_username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($forgot_username)
    {
        $this->forgot_username = $forgot_username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no_reply@sipanda-tol.ptjpt.co.id')->view('emails.test')->with([
            'nama' => $this->forgot_username
        ]);
    }
}
