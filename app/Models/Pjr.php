<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pjr extends Model
{
    public $fillable = [
        'status'
    ];

    public function dinas()
    {
        return $this->hasMany('App\Models\Dinas', 'nik', 'nik');
    }

    public function laporans()
    {
        return $this->hasMany('App\Models\Laporan', 'nik', 'user');
    }

    public function akhir_dinas()
    {
        return $this->hasMany('App\Models\AkhirDinas', 'nik', 'nik');
    }
}
