<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    public function pjrs()
    {
        return $this->belongsTo('App\Models\Pjr', 'user', 'nik');
    } 

    public function ljts()
    {
        return $this->belongsTo('App\Models\Ljt', 'user', 'nik');
    }

    public function pelapors()
    {
        return $this->belongsTo('App\Models\Pelapor', 'user', 'username');
    }
}
