<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelapor extends Model
{
    public function laporans()
    {
        return $this->hasMany('App\Models\Laporan', 'username', 'user');
    }
}