<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ljt extends Model
{
    public $fillable = [
        'status'
    ];

    public function dinas()
    {
        return $this->hasMany('App\Models\Dinas', 'nik', 'nik');
    }

    public function akhir_dinas()
    {
        return $this->hasMany('App\Models\AkhirDinas', 'nik', 'nik');
    }

    public function laporans()
    {
        return $this->hasMany('App\Models\Laporan', 'nik', 'user');
    }
}

