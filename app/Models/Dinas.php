<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dinas extends Model
{
    public function ljts()
    {
        return $this->belongsTo('App\Models\Ljt', 'nik', 'nik');
    }

    public function pjrs()
    {
        return $this->belongsTo('App\Models\Pjr', 'nik', 'nik');
    }
}
