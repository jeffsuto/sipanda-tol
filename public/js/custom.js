/*
*   Halaman Petugas -----
*   Hide and Show tabel Petugas PJR dan LJT   
*/
$(document).ready(function(e){
    $('#PJR').hide();
    $('.opt-petugas').click(function(){
        let opt = $('.opt-petugas').val();
        if(opt == 'PJR')
        {
            $('#LJT').hide();
            $('#PJR').show();
        }
        else
        {
            $('#PJR').hide();
            $('#LJT').show();
        }
    });
});

