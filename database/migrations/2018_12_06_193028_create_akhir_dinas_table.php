<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkhirDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akhir_dinas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kilometer', 5);
            $table->text('keterangan')->nullable();
            $table->enum('kendaraan', ['Patroli 217', 'Patroli 218', 'Rescue']);
            $table->enum('kondisi', ['Baik', 'Perlu Perbaikan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akhir_dinas');
    }
}
