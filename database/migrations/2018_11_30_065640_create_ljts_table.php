<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLjtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ljts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik', 10)->unique();
            $table->string('password', 8);
            $table->string('nama', 30);
            $table->string('foto', 100);
            $table->enum('jabatan', ['Rescue', 'Patroli', 'Medis']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ljts');
    }
}
