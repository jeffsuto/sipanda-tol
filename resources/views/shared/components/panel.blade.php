<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title text-bold">{{ @$title }}</h6>
        <div class="heading-elements">
            {{ @$heading_element }}
        </div>
    </div>

    <div class="panel-body">
        {{ @$body }}
    </div>
    {{ @$slot }}
</div>