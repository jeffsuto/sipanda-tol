<div id="{{ @$id }}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title text-bold">{{ @$title }}</h5>
            </div>

            <form action="{{ @$form_action }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{ @$body }}
                </div>
    
                <div class="modal-footer">
                    {{ @$footer }}
                </div>
            </form>

            {{ @$slot }}
        </div>
    </div>
</div>