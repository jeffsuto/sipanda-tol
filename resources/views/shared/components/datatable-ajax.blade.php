<table class="table datatable-basic">
    <thead>
        {{ @$thead }}
    </thead>
    <tbody id="{{ @$id }}" class="{{ @$class }}">
        {{ @$tbody }}
    </tbody>
</table>