<!-- Page header -->
<div class="page-header page-header-inverse">
		
    <!-- Page header content -->
    <div class="page-header-content">
        <div class="page-title">
            <h1 class="text-bold">SIPanda Tol</h1>
            <h6 class="text-italic">Sistem Informasi & Monitoring Pandaan Tol</h6>
        </div>
    </div>
    <!-- /page header content -->

    <!-- navbar -->
    <div class="navbar navbar-inverse navbar-transparent" id="navbar-second">
        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        @if (session()->has('IS_LOGIN'))
            <div class="navbar-collapse collapse" id="navbar-second-toggle">
                <ul class="nav navbar-nav navbar-nav-material">
                    @include('shared.partials.menu')
                </ul>
    
                <ul class="nav navbar-nav navbar-nav-material navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                                Administrator
                            <span class="caret"></span>
                        </a>
    
                        {{-- Logout --}}
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ route('logout') }}"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        @endif
    </div>
    <!-- /navbar -->

</div>
<!-- /page header -->