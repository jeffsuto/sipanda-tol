<!-- Footer -->
<div class="navbar navbar-default navbar-fixed-bottom footer">
    <ul class="nav navbar-nav visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="footer">
        <div class="row navbar-text">
            <div class="col-md-1">
                <img src="{{ asset('images/logo1.png') }}" alt="Jasa Marga" class="img-responsive">
            </div>
            <div class="col-md-1">
                <img src="{{ asset('images/logo2.png') }}" alt="Jasa Marga" class="img-responsive">
            </div>
            <div class="col-md-1">
                <img src="{{ asset('images/logo3.png') }}" alt="Jasa Marga" class="img-responsive">
            </div>
            <div class="col-md-1">
                <img src="{{ asset('images/logo4.png') }}" alt="Jasa Marga" class="img-responsive">
            </div>
        </div>
    </div>
</div>
<!-- /footer -->