{{-- Menu Halaman utama --}}
<li class="@yield('active-halaman-utama')"><a href="{{ route('halaman-utama') }}">Halaman Utama</a></li>

{{-- Menu Laporan --}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        Laporan <span class="caret"></span>
    </a>

    <ul class="dropdown-menu width-250">					
        <li>
            <a href="{{ route('laporan-petugas') }}"><i class="icon-task"></i> Petugas</a>
        </li>
        <li>
            <a href="{{ route('laporan-pelapor') }}"><i class="icon-task"></i> Pelapor</a>
        </li>
    </ul>
</li>

{{-- Menu Info Lalu Lintas --}}
<li class="@yield('active-info-lalu-lintas')"><a href="{{ route('info-lalu-lintas') }}">Info Lalu Lintas</a></li>

{{-- Menu CCTV --}}
<li class="@yield('active-cctv')"><a href="{{ route('cctv') }}">CCTV</a></li>

{{-- Menu Tarif Tol --}}
<li class="@yield('active-tarif-tol')"><a href="{{ route('tarif-tol') }}">Tarif Tol</a></li>

{{-- Menu Laporan Kerja --}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        Laporan Kerja <span class="caret"></span>
    </a>

    <ul class="dropdown-menu width-250">					
        <li>
            <a href="{{ route('laporan-kerja') }}"><i class="icon-task"></i> Detail</a>
        </li>
        <li>
            <a href="{{ route('rekap-laporan') }}"><i class="icon-task"></i> Rekap</a>
        </li>
    </ul>
</li>

{{-- Menu Petugas --}}
<li class="@yield('active-petugas')"><a href="{{ route('petugas') }}">Petugas</a></li>