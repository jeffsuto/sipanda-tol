<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Reset Password</title>
</head>
<body>
    <form action="{{ route('reset-password') }}" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <div class="form-group">
            <label class="label-control col-md-2">Password Baru</label>
            <div class="col-md-3">
                <input type="text" class="form-control" name="password">
            </div>
        </div>
        <div class="form-group">
            <label class="label-control col-md-2">Konfirmasi Password Baru</label>
            <div class="col-md-3">
                <input type="text" class="form-control" name="repassword">
            </div>
        </div>
        <button type="submit" class="btn btn-success">Kirim</button>
    </form>
</body>
</html>