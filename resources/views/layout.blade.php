<!DOCTYPE html>
<html lang="en">
    <head>
        @include('shared.partials.meta')
        @yield('title')
        @include('shared.partials.css')
        @include('shared.partials.js')
    </head>
    <body class="navbar-bottom">
        @include('shared.partials.header')
        @include('shared.partials.container')
        @include('shared.partials.footer')
    </body>
</html>