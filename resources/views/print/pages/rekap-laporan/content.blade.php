@extends('print.layout')

@section('title')
    Rekap Laporan Kerja
@endsection

@section('content')
    <div class="row">
        <h3><strong>Rekap Laporan Kerja</strong></h3>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th>NIK</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Mogok</th>
                <th>Kecelakaan</th>
                <th>Kerusakan Jalan</th>
                <th>Kebakaran</th>
                <th>Kemacetan</th>
                <th>Lain - Lain</th>
                <th>Patroli 217</th>
                <th>Patroli 218</th>
                <th>Rescue</th>
                <th>Ambulan</th>
            </thead>
            <tbody>
                @foreach ($rekaps as $rekap)
                    <tr>
                        <td>{{ $rekap['nik'] }}</td>
                        <td>{{ $rekap['nama'] }}</td>
                        <td>
                            @switch($rekap['jabatan'])
                                @case(1)
                                    Rescue
                                    @break
                                @case(2)
                                    Medis
                                    @break
                                @case(3)
                                    Patroli
                                    @break
                                @default
                                    PJR
                            @endswitch
                        </td>
                        <td>{{ $rekap['mogok'] }}</td>
                        <td>{{ $rekap['kecelakaan'] }}</td>
                        <td>{{ $rekap['kerusakan_jalan'] }}</td>
                        <td>{{ $rekap['kebakaran'] }}</td>
                        <td>{{ $rekap['kemaceten'] }}</td>
                        <td>{{ $rekap['lain'] }}</td>
                        <td>{{ $rekap['patroli_217'] }}</td>
                        <td>{{ $rekap['patroli_218'] }}</td>
                        <td>{{ $rekap['rescue'] }}</td>
                        <td>{{ $rekap['ambulan'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection