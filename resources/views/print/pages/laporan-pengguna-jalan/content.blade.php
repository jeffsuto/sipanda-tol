@extends('print.layout')

@section('title')
    Laporan Pelapor
@endsection

@section('content')
    <div class="row">
        <h3><strong>Rangkaian Laporan Pengguna Jalan</strong></h3>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th>No. Laporan</th>
                <th>Pukul</th>
                <th>Laporan</th>
                <th>Jenis</th>
                <th>Keterangan</th>
                <th>KM</th>
                <th>Arah</th>
                <th>Foto</th>
                <th>Tindak Lanjut</th>
                <th>Status Akhir</th>
            </thead>
            <tbody>
                @foreach ($laporans as $laporan)
                    <tr>
                        <td class="text-center">{{ $laporan['no'] }}</td>
                        <td>{{ $laporan['waktu'] }}</td>
                        <td>{{ $laporan['laporan'] }}</td>
                        <td>
                            @switch($laporan['jenis'])
                                @case(1)
                                    Mogok
                                    @break
                                @case(2)
                                    Kecelakaan
                                    @break
                                @case(3)
                                    Kerusakan Jalan
                                    @break
                                @case(4)
                                    Kebakaran
                                    @break
                                @case(5)
                                    Kemacetan
                                    @break
                                @default
                                    Lain - Lain
                            @endswitch
                        </td>
                        <td>{{ $laporan['keterangan'] }}</td>
                        <td>{{ $laporan['kilometer'] }}</td>
                        <td>
                            @if ($laporan['arah'] == 1)
                                Gempol
                            @else
                                Pandaan
                            @endif
                        </td>
                        <td>
                            @if ($laporan['foto'] != null)
                                <img src="{{ asset('images/laporan/'.$laporan['foto']) }}" style="width:100px" class="img-rounded img-responsive">
                            @endif
                        </td>
                        <td>{{ $laporan['tindak_lanjut'] }}</td>
                        <td>
                            @if ($laporan['status_akhir'] == 0)
                                Belum Tervalidasi
                            @else
                                Tervalidasi
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection