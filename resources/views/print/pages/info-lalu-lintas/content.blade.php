@extends('print.layout')

@section('title')
    Laporan Info Lalu Lintas
@endsection

@section('content')
    <div class="row">
        <label>Laporan Info Lalu Lintas</label><br>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th width="10%">No. Info</th>
                <th width="15%">Pukul</th>
                <th>Uraian Informasi</th>
            </thead>
            <tbody>
                @foreach ($info as $info)
                    <tr>
                        <td class="text-center">{{ $info['no'] }}</td>
                        <td>{{ $info['waktu'] }}</td>
                        <td>{{ $info['keterangan'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection