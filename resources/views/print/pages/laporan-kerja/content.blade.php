@extends('print.layout')

@section('title')
    Laporan Kerja
@endsection

@section('content')
    <div class="row">
        <label>Rangkaian Laporan</label><br>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th>No. Laporan</th>
                <th>Pukul</th>
                <th>Laporan</th>
                <th>Jenis</th>
                <th>Keterangan</th>
                <th>KM</th>
                <th>Arah</th>
                <th>Foto</th>
                <th>Tindak Lanjut</th>
                <th>Status Akhir</th>
            </thead>
            <tbody>
                @foreach ($laporan_kegiatan as $laporan)
                    <tr>
                        <td class="text-center">{{ $laporan['nomer'] }}</td>
                        <td>{{ $laporan['pukul'] }}</td>
                        <td>{{ $laporan['laporan'] }}</td>
                        <td>
                            @switch($laporan['jenis'])
                                @case(1)
                                    Mogok
                                    @break
                                @case(2)
                                    Kecelakaan
                                    @break
                                @case(3)
                                    Kerusakan Jalan
                                    @break
                                @case(4)
                                    Kebakaran
                                    @break
                                @case(5)
                                    Kemacetan
                                    @break
                                @default
                                    Lain - Lain
                            @endswitch
                        </td>
                        <td>{{ $laporan['keterangan'] }}</td>
                        <td>{{ $laporan['km'] }}</td>
                        <td>
                            @if ($laporan['arah'] == 1)
                                Gempol
                            @else
                                Pandaan
                            @endif
                        </td>
                        <td>
                            @if ($laporan['foto'] != null)
                                <img src="{{ asset('images/laporan/'.$laporan['foto']) }}" style="width:100px" class="img-rounded img-responsive">
                            @endif
                        </td>
                        <td>{{ $laporan['tindak_lanjut'] }}</td>
                        <td>
                            @if ($laporan['status_akhir'] == 0)
                                Belum Tervalidasi
                            @else
                                Tervalidasi
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{-- Rangkaian informasi lalu lintas --}}
        <label>Laporan Info Lalu Lintas</label><br>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th width="10%">No. Info</th>
                <th width="15%">Pukul</th>
                <th>Uraian Informasi</th>
            </thead>
            <tbody>
                @foreach ($info as $info)
                    <tr>
                        <td class="text-center">{{ $info['no'] }}</td>
                        <td>{{ $info['waktu'] }}</td>
                        <td>{{ $info['keterangan'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{-- Kondisi kendaraan saat akhir dinas --}}
        <label>Kondisi kendaraan saat akhir dinas</label><br>
        <table class="table table-bordered">
            <thead class="table-header-color">
                <th>Kendaraan</th>
                <th>Kilometer Awal</th>
                <th>Kilometer Awal</th>
                <th>Kilometer Jumlah</th>
                <th>Kondisi Kendaraan</th>
                <th>Keterangan</th>
            </thead>
            <tbody>
                @foreach (@$kendaraan as $item)
                    <tr>
                        <td>
                            @switch($item['kendaraan'])
                                @case(1)
                                    Patroli 217
                                    @break
                                @case(2)
                                    Patroli 218
                                    @break
                                @case(3)
                                    Rescue
                                    @break
                                @case(4)
                                    Ambulan
                                    @break
                                @case(5)
                                    PJR 211
                                @default
                                    PJR 212
                            @endswitch
                        </td>
                        <td>{{ $item['km_awal'] }}</td>
                        <td>{{ $item['km_akhir'] }}</td>
                        <td>{{ $item['km_jumlah'] }}</td>
                        <td>
                            @if ($item['kondisi'] == 1)
                                Baik
                            @else
                                Perlu Perbaikan
                            @endif
                        </td>
                        <td>{{ $item['keterangan'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection