<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @include('print.partials.css')
</head>
<body style="background-color: white" onload="window.print()">
    <div class="container-fluid">
        
        {{-- Header laporan --}}
        @include('print.partials.header')
    
        {{-- Rangkaian Laporan pengguna jalan --}}
        <div class="container-fluid" style="padding-left: 20px; padding-right: 30px">
            @yield('content')
        </div>

        {{-- Footer laporan --}}
        @include('print.partials.footer')
    </div>
</body>
</html>