<div class="row">
    <div class="col-p-6">
        <div class="row">
            <div class="col-p-2">
                <img style="width: 90px;" src="{{ asset('images/logo2.png') }}">
            </div>
            <div class="col-p-10">
                <h4 style="margin-top: 0px; margin-left:10px">
                    <b><u>Sistem Informasi & Monitoring Pandaaan Tol</u></b>
                    <br><p style="font-size: 11pt">Laporan Kegiatan Harian Operasional Pelayanan Lalu Lintas</p>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-p-12">
                <h4><strong>Kegiatan Pelayanan Lalu Lintas</strong></h4>
                <p>Tanggal &emsp;&emsp;: {{ @$tanggal }}</p>
                @empty($rekap)
                    <p>Shift &emsp;&emsp;&emsp;: {{ @$shift }}</p>
                @endempty
            </div>
        </div>
    </div>

    {{-- Karyawan yang bertugas --}}
    @empty($rekap)
        <div class="col-p-6">
            <div class="col-p-12">
                <h5><strong>Karyawan yang bertugas</strong></h5>
                <table class="table table-bordered">
                    <thead class="table-header-color">
                        <th width="5%">No</th>
                        <th width="15%">NIK</th>
                        <th>Nama</th>
                        <th width="30%">Kendaraan</th>
                    </thead>
                    <tbody>
                        @foreach ($dinas as $d)
                            <tr>
                                <td>{{ $d['no'] }}</td>
                                <td>{{ $d['nik'] }}</td>
                                <td>{{ $d['nama'] }}</td>
                                <td>
                                    @switch($d['kendaraan'])
                                        @case(1)
                                            Patroli 217
                                            @break
                                        @case(2)
                                            Patroli 218
                                            @break
                                        @case(3)
                                            Rescue
                                            @break
                                        @case(4)
                                            Ambulan
                                            @break
                                        @case(5)
                                            PJR 211
                                            @break
                                        @default
                                            PJR 212
                                    @endswitch
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endempty
</div>