@extends('layout')

@section('title')
    <title>{{ @$title }}</title>
@endsection

@section('active-info-lalu-lintas')
    {{ @$pageActive }}
@endsection

@section('content')
    <div class="col-md-10 col-md-offset-1">
        @component('shared.components.panel')
            @slot('title', 'Informasi Lalu Lintas')
            @slot('heading_element')
                @if (!empty($_GET['tanggal']) && !empty($_GET['shift']))
                    <a target="_blank" href="{{ route('print-info', 'print') }}?tanggal={{ $_GET['tanggal'] }}&shift={{ $_GET['shift'] }}" class="btn btn-primary">Cetak</a>
                @else
                    <a target="_blank" href="{{ route('print-info', 'print') }}" class="btn btn-primary">Cetak</a>
                @endif
            @endslot
            @slot('body')
                <form class="form-inline" action="{{ route('info-lalu-lintas') }}" method="get">
                    <div class="form-group">
                        <label>Tanggal : </label>
                        @if (!empty($_GET['tanggal']))
                            <input type="date" value="{{ $_GET['tanggal'] }}" name="tanggal" class="form-control" required>
                        @else
                            <input type="date" name="tanggal" class="form-control" required>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Periode Shift : </label>
                        <select name="shift" class="form-control">
                            <option value="0">Semua</option>
                            <option value="1" @if(!empty($_GET['shift']) && $_GET['shift'] == 1) selected @endif>1</option>
                            <option value="2" @if(!empty($_GET['shift']) && $_GET['shift'] == 2) selected @endif>2</option>
                            <option value="3" @if(!empty($_GET['shift']) && $_GET['shift'] == 3) selected @endif>3</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-info">Cari</button>
                </form>
            @endslot
            
            @component('shared.components.datatable-basic')
                @slot('thead')
                    <th>No. Info</th>
                    <th>Pukul</th>
                    <th>Uraian Informasi</th>
                @endslot
                
                @slot('tbody')
                    @foreach ($info as $get)
                        <tr>
                            <td class="text-center">{{ $get['no'] }}</td>
                            <td>{{ $get['waktu'] }}</td>
                            <td style="width:200px">{{ $get['keterangan'] }}</td>
                        </tr>
                    @endforeach
                @endslot
            @endcomponent
        @endcomponent
    </div>
@endsection