@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-cctv', @$pageActive)

@section('content')
    @component('shared.components.panel')
        @slot('title', 'CCTV Pandaan Tol')

        @slot('body')
            <div style="height:720px">
                <iframe src="http://jasamargalive.com" style="width:100%; height:100%;"></iframe>
            </div>
        @endslot
    @endcomponent
@endsection