@component('shared.components.modal')
    @slot('id', 'form-laporan')

    @slot('title', 'Form Laporan')

    @slot('form_action', route('add-laporan-inspeksi'))

    @slot('body')
        <div class="form-group">
            <label class="control-label col-md-3">No. Laporan</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="{{ $next_laporan }}">
            </div>
        </div>
        <input type="hidden" name="username" value="SENKOM">
        <div class="form-group">
            <label class="control-label col-md-3">Jenis</label>
            <div class="col-sm-9">
                <select name="jenis" class="form-control">
                    <option value="1">Mogok</option>
                    <option value="2">Kecelakaan</option>
                    <option value="3">Kerusakan Jalan</option>
                    <option value="4">Kebakaran</option>
                    <option value="5">Lain - lain</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Kilometer</label>
            <div class="col-sm-9">
                <input type="number" class="form-control" name="kilometer">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Arah</label>
            <div class="col-sm-9">
                <select name="arah" class="form-control">
                    <option value="1">Gempol</option>
                    <option value="2">Pandaan</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Keterangan</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="keterangan" cols="30" rows="10"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Foto</label>
            <div class="col-sm-9">
                <input type="file" name="foto" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Tindak Lanjut</label>
            <div class="col-sm-9">
                <select name="tindak_lanjut" class="form-control">
                    <option value=""></option>
                    @foreach ($pegawai as $item)
                        <option value="{{ $item['nik'] }}">{{ $item['nama'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-warning">Reset</button>
        <button type="submit" class="btn btn-success">Kirim</button>
    @endslot
@endcomponent