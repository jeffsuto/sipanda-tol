@component('shared.components.modal')
    @slot('id', 'info-lalu-lintas-modal')
    @slot('title', 'Buat Informasi Lalu Lintas')
    @slot('form_action', route('add-info-lalu-lintas'))

    @slot('body')
        <div class="form-group">
            <label class="control-label">No. Info </label>
            <input type="number" class="form-control" name="no_info" value="{{ $next_id }}">
        </div>
        <div class="form-group">
            <textarea class="form-control" cols="30" rows="10" name="keterangan"></textarea>
        </div>
    @endslot

    @slot('footer')
        <button type="submit" class="btn btn-success btn-block">Kirim</button>
    @endslot
@endcomponent