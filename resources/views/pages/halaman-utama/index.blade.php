@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-halaman-utama')
    {{ @$pageActive }}
@endsection

@section('content')
    <div class="row">
        {{-- Tabel karyawan yang bertugas --}}
        <div class="col-md-7">
            @component('shared.components.panel')
                @slot('title')
                    Karyawan yang bertugas
                @endslot
                
                @slot('body')
                    <table class="table datatable-karyawan-bertugas">
                        <thead>
                            <th class="text-center">NIK</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Jabatan Petugas</th>
                            <th class="text-center">Kendaraan</th>
                            <th class="text-center">Inspeksi</th>
                            <th class="text-center">Aksi</th>
                        </thead>
                    </table>
                @endslot
            @endcomponent
        </div>
        {{-- Tabel Informasi lalu lintas --}}
        <div class="col-md-5">
            @component('shared.components.panel')
                @slot('title')
                    Informasi Lalu Lintas
                @endslot
                @slot('heading_element')
                    <button class="btn btn-info" data-toggle="modal" data-target="#info-lalu-lintas-modal">Buat Info</button>
                @endslot
                
                @slot('body')
                    @component('shared.components.datatable-basic')
                        @slot('thead')
                            <th class="text-center">Pukul</th>
                            <th>Uraian Informasi</th>
                            <th class="text-center">Aksi</th>
                        @endslot

                        @slot('tbody')
                            @foreach (@$info as $get)
                                <tr>
                                    <td>{{ $get['created_at'] }}</td>
                                    <td>{{ $get['keterangan'] }}</td>
                                    <td>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#edit-info" onclick="editInfo('{{ $get['id'] }}')">
                                            <i class="icon-pencil"></i>
                                        </button>
                                        <a href="{{ route('delete-info', ['id' => $get['id']]) }}" class="btn btn-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                        @if ($get['status'] == 0)
                                            <a href="{{ route('activate-info', ['id' => $get['id']]) }}" class="btn btn-success">
                                                <i class="icon-check"></i>
                                            </a>
                                        @else
                                            <a href="{{ route('deactivate-info', ['id' => $get['id']]) }}" class="btn btn-warning">
                                                <i class="icon-cross"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
        {{-- Rangkaingan laporan dan kegiatan --}}
        <div class="col-md-12">
            @component('shared.components.panel')
                @slot('title')
                    Rangkaian Laporan dan Kegiatan
                @endslot

                @slot('heading_element')
                    <button class="btn btn-success" data-toggle="modal" data-target="#form-laporan">
                        Buat Laporan
                    </button>
                @endslot
                
                @slot('body')
                    <table class="table datatable-rangkaian-laporan-halaman-utama">
                        <thead>
                            <th class="text-center">No. Laporan</th>
                            <th class="text-center">Pukul</th>
                            <th class="text-center">Laporan</th>
                            <th class="text-center">Keterangan </th>
                            <th class="text-center">KM</th>
                            <th class="text-center">Arah</th>
                            <th class="text-center">Jenis</th>
                            <th class="text-center">foto</th>
                            <th class="text-center">Tindak Lanjut</th>
                            <th class="text-center">Status Akhir</th>
                            <th class="text-center">Aksi</th>
                        </thead>
                    </table>
                @endslot
            @endcomponent
        </div>
    </div>
    @include('pages.halaman-utama.modal-info-lantas')
    @include('pages.halaman-utama.modal')
    @include('pages.halaman-utama.modal-edit-laporan')
    @include('pages.halaman-utama.modal-edit-info')

    <script>
        /*
        *   fungsi untuk edit laporan
        */
        function editLaporan(id) 
        {
            $.ajax({
                'url' : '{{ url('/api/data-laporan-inspeksi') }}/'+id,
                success : function(data)
                {
                    $('.id').val(data[0].id);
                    $('.jenis').val(data[0].jenis);
                    $('.kilometer').val(data[0].kilometer);
                    $('.arah').val(data[0].arah);
                    $('.keterangan').val(data[0].keterangan);

                    if (data[0].status_akhir != '0') {
                        $('.cTindakLanjut').hide();
                        $('.cTindak').show();
                        $('.iTindak').val(data[0].tindak_lanjut);
                    }else{
                        $('.cTindak').hide();
                        $('.cTindakLanjut').show();
                    }
                }
            });    
        }

        /*
        *   fungsi untuk edit info
        */
        function editInfo(id) 
        {
            $.ajax({
                'url' : '{{ url('/api/data-info-lalu-lintas') }}/'+id,
                success : function(data)
                {
                    $('.id').val(data[0].id);
                    $('.keterangan').val(data[0].keterangan);
                }
            });
        }

        /*
        *  auto load data
        */
        setInterval(function(){
            $('.datatable-karyawan-bertugas').DataTable().ajax.reload(null, false);
            $('.datatable-rangkaian-laporan-halaman-utama').DataTable().ajax.reload(null, false);
        }, 6000);

        /* ------------------------------------------------------------------------------
        *
        *  # Datatables data sources
        *
        *  Specific JS code additions for datatable_data_sources.html page
        *
        *  Version: 1.0
        *  Latest update: Aug 1, 2015
        *
        * ---------------------------------------------------------------------------- */

        $(function() {


            // Table setup
            // ------------------------------

            // Setting datatable defaults
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                "scrollX": true,
                columnDefs: [{ 
                    orderable: true,
                    width: '100px',
                    targets: [ 5 ]
                }],
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            // table rankaian laporan halaman
            $('.datatable-rangkaian-laporan-halaman-utama').dataTable({

                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [[1, 'asc']], //Initial no order.
                ajax: {
                    "url"   : '{{ url('/api/data-rangkaian-laporan') }}'
                },
                columns: [
                    { data: "no" },
                    { data: "created_at" },
                    { data: "nama" },
                    { data: "keterangan" },
                    { data: "kilometer"},
                    { data: "arah",
                        render : function(data)
                        {
                            if (data == 1)
                                return 'Gempol';
                            else 
                                return 'Pandaan';
                        }
                    },
                    { data: "jenis", 
                        render : function(data)
                        {
                            switch (data) {
                                case '1':
                                    return 'Mogok';
                                case '2':
                                    return 'Kecelakaan';
                                case '3':
                                    return 'Kerusakan Jalan';
                                case '4':
                                    return 'Kebakaran';
                                case '5':
                                    return 'Kemacetan';
                                default:
                                    return 'Lain - lain';
                            }
                        }
                    },
                    { data: "foto", 
                        render : function(data)
                        {
                            return  '<a href="{{ asset('') }}images/laporan/'+data+'" data-popup="lightbox">'+
                                        '<img src="{{ asset('') }}images/laporan/'+data+'" alt="" class="img-rounded img-preview">'+
                                    '</a>';
                        }
                    },
                    { data: "tindak_lanjut",
                        render : function(data, type, row)
                        {
                            if (data == null || data == "") 
                            {
                                return '<form class="form-inline" action="{{ route('update-tindak-lanjut') }}" method="POST">@csrf'+
                                                '<input type="hidden" name="id" value="'+row.id+'">'+
                                                '<select name="tindak_lanjut" class="form-control">'+
                                                    '<option value=""></option>'+
                                                    '@foreach ($pegawai as $item)'+
                                                        '<option value="{{ $item['nik'] }}">{{ $item['nama'] }}</option>'+
                                                    '@endforeach'+
                                                '</select>'+
                                                '<button class="btn btn-success" type="submit">'+
                                                    '<i class="icon-paperplane"></i>'+
                                                '</button>'+
                                            '</form>';
                            }
                            else
                            {
                                return data;
                            }
                        }
                    },
                    { data: "status_akhir",
                        render : function(data)
                        {
                            if (data == 0)
                                return '<label class="label label-danger">Belum Tervalidasi</label>';
                            else
                                return '<label class="label label-success">Tervalidasi</label>';
                        }
                    },
                    { data: null,
                        render : function(data, type, row)
                        {
                            return '<button class="btn btn-primary" data-toggle="modal" data-target="#edit-laporan" onclick="editLaporan('+row.id+')">'+
                                        '<i class="icon-pencil"></i>'+
                                    '</button>'+
                                    '<form action="{{ route('delete-laporan-inspeksi') }}" method="POST">'+
                                        '@csrf'+
                                        '<input type="hidden" value="'+row.id+'" name="id">'+
                                        '<button class="btn btn-danger">'+
                                            '<i class="icon-trash"></i>'+
                                        '</button>'+
                                    '</form>';
                        }
                    }

                ],

                createdRow: function( row, data, dataIndex ) {
                    // Set the data-status attribute, and add a class
                }

            });

            // table karyawan bertugas
            $('.datatable-karyawan-bertugas').dataTable({

                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                order: [[1, 'asc']], //Initial no order.
                ajax: {
                    "url"   : '{{ url('/api/data-karyawan-bertugas') }}'
                },
                columns: [
                    { data: "nik" },
                    { data: "nama" },
                    { data: "jabatan",
                        render : function(data)
                        {
                            switch (data) {
                                case '1':
                                    return 'Rescue';
                                case '2':
                                    return 'Medis';
                                case '3':
                                    return 'Patroli';
                                case '4':
                                    return 'Medis';
                                default:
                                    return 'PJR';
                            }
                        }
                    },
                    { data: "kendaraan", 
                        render : function(data)
                        {
                            switch (data) 
                            {
                                case '1':
                                    return 'Patroli 217';
                                case '2':
                                    return 'Patroli 218';
                                case '3':
                                    return 'Rescue';
                                case '4':
                                    return 'Ambulan';
                                case '5':
                                    return 'PJR 211';
                                default:
                                    return 'PJR 212';
                            }
                        }
                    },
                    { data: "status",
                        render : function(data)
                        {
                            if (data == '2') {
                                return '<label class="label label-success">YA</label>';
                            }else{
                                return '<label class="label label-default">TIDAK</label>';
                            }
                        }
                    },
                    { data: null, 
                        render : function(data, type, row)
                        {
                            if ((row.status == '1' || row.status == '2') && row.logout_session == '0') 
                            {
                                return '<button class="btn btn-default" disabled>Logout</button>';
                            }
                            else if((row.status == '1' || row.status == '2') && row.logout_session == '1')
                            {
                                return '<a href="{{ url('logout-petugas') }}/'+row.id+'/'+row.nik+'/'+row.jabatan+'" class="btn btn-warning">Logout</a>';
                            }
                            else
                            {
                                return '<a href="{{ url('deactivate-status-dinas') }}/'+row.nik+'/'+row.jabatan+'" class="btn btn-danger">'+
                                            '<i class="icon-cross2"></i>'+
                                        '</a>'+
                                        '<a href="{{ url('activate-status-dinas') }}/'+row.nik+'/'+row.jabatan+'" class="btn btn-success">'+
                                            '<i class="icon-checkmark"></i></a>';
                            }
                        }
                    }
                ],

                createdRow: function( row, data, dataIndex ) {
                    // Set the data-status attribute, and add a class
                }

                });

            // External table additions
            // ------------------------------

            // Enable Select2 select for the length option
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });

        });
    </script>

@endsection