@component('shared.components.modal')
    @slot('id', 'edit-info')

    @slot('title', 'Update Info Lalu Lintas')

    @slot('form_action', route('update-info-lalu-lintas'))

    @slot('body')
        <input type="hidden" class="id" name="id">
        <textarea name="keterangan" class="keterangan form-control" cols="30" rows="10"></textarea>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-warning">Reset</button>
        <button type="submit" class="btn btn-success">Kirim</button>
    @endslot
@endcomponent