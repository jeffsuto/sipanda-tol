@component('shared.components.modal')
    @slot('id', 'edit-laporan')

    @slot('title', 'Update Laporan')

    @slot('form_action', route('update-laporan-inspeksi'))

    @slot('body')
        <input type="hidden" class="id" name="id">
        <div class="form-group">
            <label class="control-label col-md-3">Jenis</label>
            <div class="col-sm-9">
                <select class="jenis form-control" name="jenis">
                    <option value="1">Mogok</option>
                    <option value="2">Kecelakaan</option>
                    <option value="3">Kerusakan Jalan</option>
                    <option value="4">Kebakaran</option>
                    <option value="5">Lain - Lain</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Kilometer</label>
            <div class="col-sm-9">
                <input type="text" name="kilometer" class="form-control kilometer">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Arah</label>
            <div class="col-sm-9">
                <select class="arah form-control" name="arah">
                    <option value="1">Gempol</option>
                    <option value="2">Pandaan</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Keterangan</label>
            <div class="col-sm-9">
                <textarea class="form-control keterangan" name="keterangan" cols="30" rows="10"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Tindak Lanjut</label>
            <div class="col-sm-9 cTindak">
                <input type="text" name="itindak_lanjut" class="form-control iTindak" readonly>
            </div>
            <div class="col-sm-9 cTindakLanjut">
                <select name="tindak_lanjut" class="tindak_lanjut form-control">
                    <option value=""></option>
                    @foreach ($pegawai as $item)
                        <option value="{{ $item['nik'] }}">{{ $item['nama'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-warning">Reset</button>
        <button type="submit" class="btn btn-success">Kirim</button>
    @endslot

@endcomponent