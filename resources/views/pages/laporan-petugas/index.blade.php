@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection
      
@section('content')
    @component('shared.components.panel')
        @slot('title', 'Rankaian Laporan Petugas')

        @slot('heading_element')
            @if (!empty($_GET['tanggal']) && !empty($_GET['shift']) && !empty($_GET['jenis']))
                <a target="_blank" href="{{ route('print-laporan-petugas', 'print') }}?tanggal={{ $_GET['tanggal'] }}&shift={{ $_GET['shift'] }}&jenis={{ $_GET['jenis'] }}" class="btn btn-primary">Cetak</a>
            @else
                <a target="_blank" href="{{ route('print-laporan-petugas', 'print') }}" class="btn btn-primary">Cetak</a>
            @endif
        @endslot

        @slot('body')
            <form action="{{ route('laporan-petugas') }}" class="form-inline" method="get">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" name="tanggal" @if(!empty($_GET['tanggal'])) value="{{ $_GET['tanggal'] }}" @endif class="form-control">
                </div>
                <div class="form-group">
                    <label>Periode Shift</label>
                    <select name="shift" class="form-control">
                        <option value="0" @if(!empty($_GET['shift']) && $_GET['shift'] == '0') selected @endif>Semua</option>
                        <option value="1" @if(!empty($_GET['shift']) && $_GET['shift'] == '1') selected @endif>1</option>
                        <option value="2" @if(!empty($_GET['shift']) && $_GET['shift'] == '2') selected @endif>2</option>
                        <option value="3" @if(!empty($_GET['shift']) && $_GET['shift'] == '3') selected @endif>3</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Jenis</label>
                    <select name="jenis" class="form-control">
                        <option value="0" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '0') selected @endif>Semua</option>
                        <option value="1" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '1') selected @endif>Mogok</option>
                        <option value="2" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '2') selected @endif>Kecelakan</option>
                        <option value="3" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '3') selected @endif>Kerusakan Jalan</option>
                        <option value="4" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '4') selected @endif>Kebakaran</option>
                        <option value="5" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '5') selected @endif>Kemacetan</option>
                        <option value="6" @if(!empty($_GET['jenis']) && $_GET['jenis'] == '6') selected @endif>Lain - Lain</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info">Cari</button>
                </div>
            </form>
        @endslot

        @component('shared.components.datatable-basic')
            @slot('thead')
                <th>No. Laporan</th>
                <th>Pukul</th>
                <th>Laporan</th>
                <th>Jenis</th>
                <th>Keterangan</th>
                <th>KM</th>
                <th>Arah</th>
                <th>Foto</th>
                <th>Tindak Lanjut</th>
                <th>Status Akhir</th>
            @endslot

            @slot('tbody')
                @foreach ($laporans as $laporan)
                    <tr>
                        <td class="text-center">{{ $laporan['no'] }}</td>
                        <td>{{ $laporan['waktu'] }}</td>
                        <td>{{ $laporan['laporan'] }}</td>
                        <td>
                            @switch($laporan['jenis'])
                                @case(1)
                                    Mogok
                                    @break
                                @case(2)
                                    Kecelakaan
                                    @break
                                @case(3)
                                    Kerusakan Jalan
                                    @break
                                @case(4)
                                    Kebakaran
                                    @break
                                @case(5)
                                    Kemacetan
                                    @break
                                @default
                                    Lain - Lain
                            @endswitch
                        </td>
                        <td>{{ $laporan['keterangan'] }}</td>
                        <td>{{ $laporan['kilometer'] }}</td>
                        <td>
                            @if ($laporan['arah'] == 1)
                                Gempol
                            @else
                                Pandaan
                            @endif
                        </td>
                        <td>
                            <a href="{{ asset('images/laporan/'.$laporan['foto']) }}" data-popup="lightbox">
                                <img src="{{ asset('images/laporan/'.$laporan['foto']) }}" alt="" class="img-rounded img-preview">
                            </a>
                        </td>
                        <td>{{ $laporan['tindak_lanjut'] }}</td>
                        <td>
                            @if ($laporan['status_akhir'] == 0)
                                <label class="label label-danger">Belum Tervalidasi</label>
                            @else
                                <label class="label label-success">Tervalidasi</label>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @endcomponent
@endsection