@component('shared.components.modal')
    @slot('id', 'edit-petugas-modal')
    @slot('title', 'Edit Petugas')
    @slot('form_action', route('update-petugas'))

    @slot('body')
        <input type="hidden" class="id" name="id">
        <div class="form-group">
            <label class="label-control col-sm-2">NIK</label>
            <div class="col-sm-10">
                <input type="number" id="nik" class="nik form-control" name="nik">
            </div>
        </div>
        <div class="form-group">
            <label class="label-control col-sm-2">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="nama form-control" name="nama">
            </div>
        </div>
        <div class="form-group group-petugas">
            <label class="label-control col-sm-2">Petugas</label>
            <div class="col-sm-10">
                <select name="petugas" class="petugas form-control">
                    <option value="LJT">LJT</option>
                    <option value="PJR">PJR</option>
                </select>
            </div>
        </div>
        <div class="form-group c-jabatan">
            <label class="label-control col-sm-2">Jabatan</label>
            <div class="col-sm-10">
                <select name="jabatan" class="jabatan form-control">
                    <option value="1">Rescue</option>
                    <option value="2">Medis</option>
                    <option value="3">Patroli</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="label-control col-sm-2">Foto</label>
            <div class="col-sm-10">
                <input type="file" name="foto" class="form-control">
            </div>
        </div>

        @slot('footer')
            <button type="submit" class="btn btn-success">Save</button>
        @endslot
    @endslot

    <script>
        $('.group-petugas').hide();
    </script>
@endcomponent