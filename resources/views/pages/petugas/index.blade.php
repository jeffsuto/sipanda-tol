@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-petugas', @$pageActive)

@section('content')
    @component('shared.components.panel')
        @slot('title')
            Daftar Petugas
        @endslot

        @slot('heading_element')
            <div class="heading-btn">
                <button class="btn btn-info" data-toggle="modal" data-target="#petugas-modal">Tambah Petugas</button>
                <form class="heading-form">
                    <select class="opt-petugas form-control">
                        <option value="LJT">LJT</option>
                        <option value="PJR">PJR</option>
                    </select>
                </form>
            </div>
        @endslot

        @slot('body')
            <div class="col-md-12" id="LJT">
                @component('shared.components.panel')
                    @slot('title')
                        Daftar Petugas Pelayanan Lalu Lintas
                    @endslot
        
                    @slot('body')
                        @component('shared.components.datatable-basic')
                            @slot('thead')
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            @endslot

                            @slot('tbody')
                                @foreach ($ljt as $get)
                                    <tr>
                                        <td>{{ $get['nik'] }}</td>
                                        <td>{{ $get['nama'] }}</td>
                                        <td>
                                            @switch($get['jabatan'])
                                                @case(1)
                                                    Rescue
                                                    @break
                                                @case(2)
                                                    Medis
                                                    @break
                                                @default
                                                    Patroli
                                            @endswitch
                                        </td>
                                        <td class="text-center">
                                            <img style="height:25%" src="{{ asset('images/users/'.$get['foto']) }}" class="img-thumbnail img-responsive">
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" data-toggle="modal" 
                                                    data-target="#edit-petugas-modal" onclick="editPetugas('{{ $get['id'] }}', 'LJT')">
                                                <i class="icon-pencil"></i>
                                            </button>
                                            <form action="{{ route('delete-petugas', ['role' => 'LJT']) }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $get['id'] }}">
                                                <button class="btn btn-danger">
                                                    <i class="icon-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
            </div>

            <div class="col-md-12" id="PJR">
                @component('shared.components.panel')
                    @slot('title')
                        Daftar Petugas PJR
                    @endslot
        
                    @slot('body')
                        @component('shared.components.datatable-basic')
                            @slot('thead')
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            @endslot
            
                            @slot('tbody')
                                @foreach ($pjr as $get)
                                    <tr>
                                        <td>{{ $get['nik'] }}</td>
                                        <td>{{ $get['nama'] }}</td>
                                        <td>PJR</td>
                                        <td>
                                            <img style="height:5%" src="{{ asset('images/users/'.$get['foto']) }}" class="img-thumbnail img-responsive">
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" data-toggle="modal" 
                                                data-target="#edit-petugas-modal" onclick="editPetugas('{{ $get['id'] }}', 'PJR')">
                                                <i class="icon-pencil"></i>
                                            </button>
                                            <form action="{{ route('delete-petugas', ['role' => 'PJR']) }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $get['id'] }}">
                                                <button class="btn btn-danger">
                                                    <i class="icon-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
            </div>
        @endslot
    @endcomponent
    @include('pages.petugas.modal-add-petugas')
    @include('pages.petugas.modal-edit-petugas')
    
    <script>

        function editPetugas(id, role) {
            $.ajax({
                'url' : '{{ url('/api/data-petugas') }}/'+id+'/'+role,
                success : function(data)
                {
                    $('.id').val(data[0].id);
                    $('.nik').val(data[0].nik);
                    $('.nama').val(data[0].nama);
                    $('.petugas').val(role);
                    
                    if (role == 'LJT') 
                    {
                        $('.jabatan').val(data[0].jabatan);
                    }
                    else
                    {
                        $('.c-jabatan').remove()
                    }
                },
                error : function(data)
                {
                    console.log("Bad Connection");
                }
            })
        }

    </script>
@endsection