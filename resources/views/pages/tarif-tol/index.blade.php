@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-tarif-tol')
    {{ @$pageActive }}
@endsection

@section('content')
    <div class="col-md-6 col-md-offset-3">
        @component('shared.components.panel')
            @slot('title')
                Tarif Pandaan Tol
            @endslot

            @slot('body')
                <form action="{{ route('add-tarif') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        @isset($image['foto'])
                            <img src="{{ asset('/images/tarif/'.$image['foto']) }}" class="img-thumbnail img-responsive">
                            <hr>
                        @endisset
                    </div>
                    <div class="form-group">
                        <label class="text-bold">UPLOAD TARIF TERBARU</label>
                        <input type="file" name="foto" class="form-control" onchange="loadFile(event)">
                    </div>
                    <div class="form-group">
                        <img id="img-preview" class="img-thumbnail img-responsive">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </form>
            @endslot
        @endcomponent
    </div>
@endsection
<script>

    // image preview
    var loadFile = function(event) {
        var output = document.getElementById('img-preview');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

</script>