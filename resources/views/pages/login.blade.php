@extends('layout')

@section('title')
    <title>Login</title>
@endsection

@section('content')
    <div class="col-md-4 col-md-offset-4">
        @component('shared.components.panel')
            @slot('title', 'Login')

            @slot('body')
                <form action="{{ route('check-login') }}" method="post" class="form-horizontal">
                    @csrf
                    <div class="form-group">
                        <label class="label-control col-sm-3">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="label-control col-sm-3">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Login</button>
                    </div>
                </form>
            @endslot
        @endcomponent
    </div>
@endsection