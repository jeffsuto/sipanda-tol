@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-laporan-kerja', @$pageActive)

@section('content')
    <h6 class="content-group text-semibold">
        Rekap Laporan Kerja
        <small>
            <form action="{{ route('rekap-laporan') }}" class="form-inline" method="get">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" @if(!empty($_GET['tanggal_dari'])) value="{{ $_GET['tanggal_dari'] }}" @endif name="tanggal_dari" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>s/d</label>
                    <input type="date" @if(!empty($_GET['tanggal_sampai'])) value="{{ $_GET['tanggal_sampai'] }}" @endif name="tanggal_sampai" class="form-control" required>
                </div>
                <div class="form-group">
                    {{-- Filter petugas --}}
                    <label>Petugas</label>
                    <select name="petugas" class="petugas form-control" onchange="selectJabatan()">
                        <option value="0">Semua</option>
                        <option value="LJT" @if(!empty($_GET['petugas']) && $_GET['petugas'] == 'LJT') selected @endif>LJT</option>
                        <option value="PJR" @if(!empty($_GET['petugas']) && $_GET['petugas'] == 'PJR') selected @endif>PJR</option>
                    </select>

                    {{-- Filter Jabatan --}}
                    <label>Jabatan</label>
                    <select name="jabatan" class="jabatan form-control">
                        <option value="0">Semua</option>
                        <option class="jbt-ljt" value="1" @if(!empty($_GET['jabatan']) && $_GET['jabatan'] == '1') selected @endif>Rescue</option>
                        <option class="jbt-ljt" value="2" @if(!empty($_GET['jabatan']) && $_GET['jabatan'] == '2') selected @endif>Medis</option>
                        <option class="jbt-ljt" value="3" @if(!empty($_GET['jabatan']) && $_GET['jabatan'] == '3') selected @endif>Patroli</option>
                        <option class="jbt-pjr" value="4" @if(!empty($_GET['jabatan']) && $_GET['jabatan'] == '4') selected @endif>PJR</option>
                    </select>
                    
                    {{-- Filter nama --}}
                    <label>Nama</label>
                    <select name="nama" class="form-control">
                        <option value="0">Semua</option>
                        @foreach ($ljts as $ljt)
                            <option class="nama-ljt" value="{{ $ljt['nik'] }}" @if(!empty($_GET['nama']) && $_GET['nama'] == $ljt['nik']) selected @endif>{{ $ljt['nama'] }}</option>
                        @endforeach
                        @foreach ($pjrs as $pjr)
                            <option class="nama-pjr" value="{{ $pjr['nik'] }}" @if(!empty($_GET['nama']) && $_GET['nama'] == $pjr['nik']) selected @endif>{{ $pjr['nama'] }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-info">Cari</button>

                    {{-- Button cetak --}}
                    @if (!empty($_GET['tanggal_dari']) && !empty($_GET['tanggal_sampai']))
                        <a target="_blank" href="{{ route('print-rekap-laporan', 'print') }}
                                   ?tanggal_dari={{ $_GET['tanggal_dari'] }}
                                   &tanggal_sampai={{ $_GET['tanggal_sampai'] }}
                                   &petugas={{ $_GET['petugas'] }}
                                   &jabatan={{ $_GET['jabatan'] }}
                                   &nama={{ $_GET['nama'] }}" 
                                   class="btn btn-primary">
                            Cetak
                        </a>
                    @else
                        <a target="_blank" href="{{ route('print-rekap-laporan', 'print') }}" class="btn btn-primary">Cetak</a>
                    @endif
                </div>
            </form>
        </small>
    </h6>
    
    @component('shared.components.panel')
        @slot('body')
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Mogok</th>
                        <th>Kecelakaan</th>
                        <th>Kerusakan Jalan</th>
                        <th>Kebakaran</th>
                        <th>Kemacetan</th>
                        <th>Lain - Lain</th>
                        <th>217</th>
                        <th>218</th>
                        <th>Rescue</th>
                        <th>Ambulan</th>
                        <th>PJR 211</th>
                        <th>PJR 212</th>
                    </thead>
                    <tbody>
                        @foreach ($rekaps as $rekap)
                            <tr>
                                <td>{{ $rekap['nik'] }}</td>
                                <td>{{ $rekap['nama'] }}</td>
                                <td>
                                    @switch($rekap['jabatan'])
                                        @case(1)
                                            Rescue
                                            @break
                                        @case(2)
                                            Medis
                                            @break
                                        @case(3)
                                            Patroli
                                            @break
                                        @default
                                            PJR
                                    @endswitch
                                </td>
                                <td>{{ $rekap['mogok'] }}</td>
                                <td>{{ $rekap['kecelakaan'] }}</td>
                                <td>{{ $rekap['kerusakan_jalan'] }}</td>
                                <td>{{ $rekap['kebakaran'] }}</td>
                                <td>{{ $rekap['kemacetan'] }}</td>
                                <td>{{ $rekap['lain'] }}</td>
                                <td>{{ $rekap['patroli_217'] }}</td>
                                <td>{{ $rekap['patroli_218'] }}</td>
                                <td>{{ $rekap['rescue'] }}</td>
                                <td>{{ $rekap['ambulan'] }}</td>
                                <td>{{ $rekap['pjr_211'] }}</td>
                                <td>{{ $rekap['pjr_212'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endslot
    @endcomponent
    
    <script>
        let urlParams = new URLSearchParams(window.location.search);
        let urlPetugas = urlParams.get('petugas');
        
        selectJabatan();
        function selectJabatan() 
        {
            let petugas = $('.petugas').val();

            if ((petugas == 'PJR' && (urlPetugas == 'PJR' || urlPetugas == undefined)) || (petugas == 'PJR' && (urlPetugas == 'LJT' || urlPetugas == '0'))) {
                $('.jbt-pjr').show();
                $('.nama-pjr').show();

                $('.jbt-ljt').hide();
                $('.nama-ljt').hide();
            }else if((petugas == 'LJT' && (urlPetugas == 'LJT' || urlPetugas == undefined)) || (petugas == 'LJT' && (urlPetugas == 'PJR' || urlPetugas == '0'))){
                $('.jbt-ljt').show();
                $('.nama-ljt').show();

                $('.jbt-pjr').hide();
                $('.nama-pjr').hide();
            }   
        }

    </script>
@endsection