@extends('layout')

@section('title')
    <title>{{ @$title }} | Home</title>
@endsection

@section('active-laporan-kerja', @$pageActive)

@section('content')
    <h6 class="content-group text-semibold">
        Laporan Kerja
        <small>
            <form action="{{ route('laporan-kerja') }}" class="form-inline" method="get">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" @if(!empty($_GET['tanggal'])) value="{{ $_GET['tanggal'] }}" @endif class="form-control" name="tanggal">
                </div>
                <div class="form-group">
                    <label>Periode Shift</label>
                    <select name="shift" class="form-control">
                        <option value="1" @if(!empty($_GET['shift']) && $_GET['tanggal'] == 1) selected @endif>1</option>
                        <option value="2" @if(!empty($_GET['shift']) && $_GET['tanggal'] == 2) selected @endif>2</option>
                        <option value="3" @if(!empty($_GET['shift']) && $_GET['tanggal'] == 3) selected @endif>3</option>
                    </select>
                    <button type="submit" class="btn btn-info">Cari</button>

                    {{-- Button cetak --}}
                    @if (!empty($_GET['tanggal']) && !empty($_GET['shift']))
                        <a target="_blank" href="{{ route('print-detail-laporan-kerja', 'print') }}?tanggal={{ $_GET['tanggal'] }}&shift={{ $_GET['shift'] }}" class="btn btn-primary">Cetak</a>
                    @else
                        <a target="_blank" href="{{ route('print-detail-laporan-kerja', 'print') }}" class="btn btn-primary">Cetak</a>
                    @endif
                </div>
            </form>
        </small>
    </h6>
    
    {{-- Panel Karyawan yang bertugas --}}
    <div class="col-md-7">
        @component('shared.components.panel')
            @slot('title', 'Karyawan yang bertugas')

            @slot('body')
                @component('shared.components.datatable-basic')
                    @slot('thead')
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Jabatan Petugas</th>
                        <th class="text-center">Kendaraan</th>
                    @endslot

                    @slot('tbody')
                        @foreach (@$dinas as $get)
                            <tr>
                                <td>{{ $get['nik'] }}</td>
                                <td>{{ $get['nama'] }}</td>
                                <td>
                                    @switch($get['jabatan'])
                                        @case(1)
                                            Rescue
                                            @break
                                        @case(2)
                                            Medis
                                            @break
                                        @case(3)
                                            Patroli
                                            @break
                                        @default
                                            PJR
                                    @endswitch
                                </td>
                                <td>
                                    @switch($get['kendaraan'])
                                        @case(1)
                                            Patroli 217
                                            @break
                                        @case(2)
                                            Patroli 218
                                            @break
                                        @case(3)
                                            Rescue
                                            @break
                                        @case(4)
                                            Ambulan
                                            @break
                                        @case(5)
                                            PJR 211
                                            @break
                                        @default
                                            PJR 212
                                    @endswitch
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>

    {{-- Panel Informasi Lalu lintas --}}
    <div class="col-md-5">
        @component('shared.components.panel')
            @slot('title', 'Informasi Lalu Lintas')
    
            @slot('body')
                @component('shared.components.datatable-basic')
                    @slot('thead')
                        <th>Pukul</th>
                        <th>Uraian Informasi</th>
                    @endslot

                    @slot('tbody')
                        @foreach (@$info as $get)
                            <tr>
                                <td>{{ $get['waktu'] }}</td>
                                <td>{{ $get['keterangan'] }}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>

    {{-- Panel Laporan Rangkaian kegiatan --}}
    <div class="col-md-12">
        @component('shared.components.panel')
            @slot('title', 'Laporan Rangkaian Kegiatan')
            
            @slot('body')
                @component('shared.components.datatable-basic')
                    @slot('thead')
                        <th>No. Laporan</th>
                        <th>Pukul</th>
                        <th>Laporan</th>
                        <th>Jenis</th>
                        <th>Keterangan</th>
                        <th>KM</th>
                        <th>Arah</th>
                        <th>Foto</th>
                        <th>Tindak Lanjut</th>
                        <th>Status Akhir</th>
                    @endslot

                    @slot('tbody')
                        @forelse ($laporan_kegiatan as $get)
                            <tr>
                                <td class="text-center">{{ $get['nomer'] }}</td>
                                <td>{{ $get['pukul'] }}</td>
                                <td>{{ $get['laporan'] }}</td>
                                <td>
                                    @switch($get['jenis'])
                                        @case(1)
                                            Mogok
                                            @break
                                        @case(2)
                                            Kecelakaan
                                            @break
                                        @case(3)
                                            Kerusakan Jalan
                                            @break
                                        @case(4)
                                            Kebakaran
                                            @break
                                        @case(5)
                                            Kemacetan
                                            @break
                                        @default
                                            Lain - Lain
                                    @endswitch
                                </td>
                                <td>{{ $get['keterangan'] }}</td>
                                <td>{{ $get['km'] }}</td>
                                <td>
                                    @switch($get['arah'])
                                        @case(1)
                                            Gempol
                                            @break
                                        @case(2)
                                            Pandaan
                                            @break
                                        @default
                                            
                                    @endswitch
                                </td>
                                <td>
                                    <a href="{{ asset('images/laporan/'.$get['foto']) }}" data-popup="lightbox">
                                        <img src="{{ asset('images/laporan/'.$get['foto']) }}" alt="" class="img-rounded img-preview">
                                    </a>
                                </td>
                                <td>{{ $get['tindak_lanjut'] }}</td>
                                <td>
                                    @if ($get['status_akhir'] == 0)
                                        <label class="label label-danger">Belum Tervalidasi</label>
                                    @else
                                        <label class="label label-success">Tervalidasi</label>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>

    {{-- Panel Kondisi Kendaraan Saat Dinas Akhir Shift --}}
    <div class="col-md-12">
        @component('shared.components.panel')
            @slot('title', 'Kondisi Kendaraan Saat Dinas Akhir')
        
            @slot('body')
                @component('shared.components.datatable-basic')
                    @slot('thead')
                        <th>Kendaraan</th>
                        <th>Kilometer Awal</th>
                        <th>Kilometer Awal</th>
                        <th>Kilometer Jumlah</th>
                        <th>Kondisi Kendaraan</th>
                        <th>Keterangan</th>
                    @endslot

                    @slot('tbody')
                        @foreach (@$kendaraan as $item)
                            <tr>
                                <td>
                                    @switch($item['kendaraan'])
                                        @case(1)
                                            Patroli 217
                                            @break
                                        @case(2)
                                            Patroli 218
                                            @break
                                        @case(3)
                                            Rescue
                                            @break
                                        @default
                                            Ambulan
                                    @endswitch
                                </td>
                                <td>{{ $item['km_awal'] }}</td>
                                <td>{{ $item['km_akhir'] }}</td>
                                <td>{{ $item['km_jumlah'] }}</td>
                                <td>
                                    @if ($item['kondisi'] == 1)
                                        Baik
                                    @else
                                        Perlu Perbaikan
                                    @endif
                                </td>
                                <td>{{ $item['keterangan'] }}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>
@endsection