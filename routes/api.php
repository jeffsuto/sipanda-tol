<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// routing REST API for mobile
Route::namespace('Mobile')->group(function() {
    Route::post('/mobile-login', 'UserController@login');
    Route::post('/show-user', 'UserController@show');
    Route::post('/register-pelapor', 'PelaporController@create');
    Route::post('/create-laporan-inspeksi', 'LaporanInspeksiController@create');
    Route::post('/check-status-dinas', 'PegawaiController@checkStatusDinas');
    Route::post('/check-login', 'PegawaiController@checkLogin');
    Route::post('/create-akhir-dinas', 'AkhirDinasController@create');
    Route::post('/update-status-inspeksi', 'PegawaiController@updateInspeksi');
    Route::post('/check-existential-user', 'UserController@checkExistential');
    Route::post('/create-dinas', 'DinasController@create');
    Route::get('/data-laporan-inspeksi/{id}', 'LaporanInspeksiController@getLaporanInspeksi')->name('data-laporan-inspeksi');
    Route::post('/data-laporan-baru', 'LaporanInspeksiController@getLaporanBaru');
    Route::post('/data-laporan-validasi', 'LaporanInspeksiController@getLaporanValidasi');
    Route::post('/data-riwayat-laporan', 'LaporanInspeksiController@getRiwayatLaporan');
    Route::post('/update-status-laporan', 'LaporanInspeksiController@updateStatusAkhir');
    Route::post('/update-profile-pelapor', 'PelaporController@update');
    Route::post('/last-data-akhir-dinas', 'AkhirDinasController@getLastDataAkhirDinasByNik');
    Route::post('/lupa-username', 'UserController@lupaUsername');
    Route::post('/reset-password', 'UserController@resetPassword');
    Route::post('/update-profile', 'UserController@updateProfile');
    Route::post('/data-kilometer-akhir-kendaraan', 'AkhirDinasController@getLastKilometerVehicle');
});

// Get data info lalu lintas
Route::namespace('Pages')->group(function(){
    Route::get('/data-info-lalu-lintas', 'InfoLaluLintasController@showInfoLaluLintas');
    Route::get('/data-info-lalu-lintas/{id}', 'InfoLaluLintasController@showInfoLaluLintasById');
    Route::get('/data-petugas/{id}/{role}', 'PetugasController@getDataPetugas')->name('data-petugas');
    Route::get('/data-karyawan-bertugas', 'HalamanUtamaController@getKaryawanYangBertugas');
    Route::get('/data-rangkaian-laporan', 'HalamanUtamaController@getRangkaianLaporan');
});
