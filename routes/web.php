<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// routing pages
Route::namespace('Pages')->group(function() {

    /**
     * Pages routes
     */
    Route::get('/', 'HalamanUtamaController@index')->name('halaman-utama')->middleware('login');
    Route::get('/tarif-tol', 'TarifTolController@index')->name('tarif-tol')->middleware('login');
    Route::get('/info-lalu-lintas', 'InfoLaluLintasController@index')->name('info-lalu-lintas')->middleware('login');
    Route::get('/cctv', 'CctvController@index')->name('cctv')->middleware('login');
    Route::get('/detail-laporan-kerja', 'LaporanKerjaController@index')->name('laporan-kerja')->middleware('login');
    Route::get('/rekap-laporan-kerja', 'RekapLaporanController@index')->name('rekap-laporan')->middleware('login');
    Route::get('/laporan', 'LaporanPetugasController@index')->name('laporan-petugas')->middleware('login');
    Route::get('/laporan/petugas', 'LaporanPetugasController@index')->name('laporan-petugas')->middleware('login');
    Route::get('/laporan/pelapor', 'LaporanPelaporController@index')->name('laporan-pelapor')->middleware('login');
    Route::get('/petugas', 'PetugasController@index')->name('petugas')->middleware('login');
    Route::get('/login', function(){
        return view('pages.login');
    })->name('login');

    Route::post('/authentication-senkom', 'LoginController@checkLogin')->name('check-login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    
    /**
     * Action routes
     */
    // print rekap laporan kerja
    Route::get('rekap-laporan-kerja/{print}', 'RekapLaporanController@index')->name('print-rekap-laporan')->middleware('login');

    // print detail laporan kerja
    Route::get('detail-laporan-kerja/{print}', 'LaporanKerjaController@index')->name('print-detail-laporan-kerja')->middleware('login');

    // print info lalu lintas
    Route::get('info-lalu-lintas/{print}', 'InfoLaluLintasController@index')->name('print-info')->middleware('login');

    // print laporan pelapor
    Route::get('laporan/pelapor/{print}', 'LaporanPelaporController@index')->name('print-laporan-pelapor')->middleware('login');

    // print laporan pelapor
    Route::get('laporan/petugas/{print}', 'LaporanPetugasController@index')->name('print-laporan-petugas')->middleware('login');

    // delete info dari halaman utama
    Route::get('delete-info-lalu-lintas/{id}', 'HalamanUtamaController@deleteInfo')->name('delete-info')->middleware('login');
    
    // mengaktiftkan status dinas pegawai
    Route::get('/activate-status-dinas/{nik}/{role}', 'HalamanUtamaController@activateStatusPegawai')->name('activate-pegawai')->middleware('login');
    
    // menonaktfkan status dinas pegawai
    Route::get('/deactivate-status-dinas/{nik}/{role}', 'HalamanUtamaController@deactivateStatusPegawai')->name('deactivate-pegawai')->middleware('login');
    
    // menonaktifkan info lalu lintas
    Route::get('/deactivate-info/{id}', 'InfoLaluLintasController@deactivateInfo')->name('deactivate-info')->middleware('login');

    // mengaktifkan info lalu lintas
    Route::get('/activate-info/{id}', 'InfoLaluLintasController@activateInfo')->name('activate-info')->middleware('login');

    // logout petugas
    Route::get('/logout-petugas/{id}/{nik}/{role}', 'HalamanUtamaController@logoutPegawai')->name('logout-pegawai')->middleware('login');

    // POST Request ----------------
    Route::post('/add-laporan-inspeksi', 'HalamanUtamaController@createLaporanInspeksi')->name('add-laporan-inspeksi')->middleware('login');
    Route::post('/update-laporan-inspeksi', 'HalamanUtamaController@updateLaporanInspeksi')->name('update-laporan-inspeksi')->middleware('login');
    
    Route::post('/add-petugas', 'PetugasController@create')->name('add-petugas')->middleware('login');

    // Update petugas
    Route::post('/update-petugas', 'PetugasController@update')->name('update-petugas')->middleware('login');

    // hapus petugas
    Route::post('/delete-petugas/{role}', 'PetugasController@delete')->name('delete-petugas')->middleware('login');
    
    Route::post('/add-info-lalu-lintas', 'InfoLaluLintasController@create')->name('add-info-lalu-lintas')->middleware('login');
    Route::post('/update-info-lalu-lintas', 'InfoLaluLintasController@update')->name('update-info-lalu-lintas')->middleware('login');

    Route::post('/add-tarif', 'TarifTolController@store')->name('add-tarif')->middleware('login');
    Route::post('/update-tindak-lanjut', 'HalamanUtamaController@updateTindakLanjut')->name('update-tindak-lanjut')->middleware('login');
    Route::post('/delete-laporan-inspeksi', 'HalamanUtamaController@deleteLaporanInspeksi')->name('delete-laporan-inspeksi')->middleware('login');
});

/**
 * Reset password route
 */
Route::post('/reset-password', 'Mobile\UserController@resetPassword')->name('reset-password');